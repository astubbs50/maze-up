﻿var shapes = [
	[
		[0, 0, 0],
		[0, 0, 0],
		[0, 0, 0]
	],
	[
		[1, 0, 1],
		[1, 0, 1],
		[1, 0, 1]
	],
	[
		[1, 1, 1],
		[0, 0, 1],
		[1, 0, 1]
	],
	[
		[1, 0, 1],
		[0, 0, 0],
		[1, 0, 1]
	],
	[
		[1, 1, 1],
		[0, 0, 1],
		[1, 1, 1]
	],
	[
		[1, 1, 1],
		[0, 0, 0],
		[1, 0, 1]
	],
	[
		[1, 1, 1],
		[1, 1, 1],
		[1, 1, 1]
	]
];

function CopyShape(shape)
{
	var newShape = [];
	for(var i = 0; i < shape.length; i++)
	{
		var newShapeOuter = [];
		for(var j = 0; j < shape[i].length; j++)
		{
			newShapeOuter.push(shape[i][j]);
		}
		newShape.push(newShapeOuter);
	}
	
	return newShape;
}

function RotateShape(shape)
{
	var tempShape = CopyShape(shape);
	var n = tempShape.length;
	
	for(var x = 0; x < tempShape[0].length; x++)
	{
		for(var y = 0; y < n; y++)
		{
			tempShape[x][y] = shape[n - y - 1][x];
		}
	}	
	
	return tempShape;
}
