var touchTimer = 0;
var touchTimerDefault = 10;
var touchStartX;
var touchStartY;
var touchX;
var touchY;

var mouseX;
var mouseY;
var lastMouseX;
var lastMouseY;
var mouseDown;
var divTouchPad;
//var preventDoubleClick = false;
var menuPlayDepressed = false;
var menuOptionsDepressed = false;
var nextLevelButtonEnabled = false;
var menuButtonEnabled = true;
var resumeGameTimeout = -1;
var levelNumberClicked = -1;

function InitTouchPad()
{
    $("img").each(function ()
    {
        $(this)[0].ondragstart = function (e) { return e.preventDefault(); };
        $(this)[0].ondrop = function (e) { return e.preventDefault(); };
    });

    document.onselectstart = function () { return false; };
    document.onContextMenu = function () { return false; };
    document.oncontextmenu = function () { return false; };

    ConfigureSizes();

    InitTouchButton("imgLeft", TouchLeft, TouchLeftEnd);
    InitTouchButton("imgUp1", TouchUp, TouchUpEnd);
    InitTouchButton("imgUp2", TouchUp, TouchUpEnd);
    InitTouchButton("imgRight", TouchRight, TouchRightEnd);
    InitTouchButton("imgDown1", TouchDown, TouchDownEnd);
    InitTouchButton("imgDown2", TouchDown, TouchDownEnd);
    InitTouchButton("imgPlayAgain", PlayAgainButtonDown, PlayAgainButtonUp);
    InitTouchButton("imgNextLevel", nextLevelButtonDown, nextLevelButtonUp);
    InitTouchButton("imgMenu", MenuButtonDown, MenuButtonUp);
    InitTouchButton("imgPause", PauseButtonDown, PauseButtonUp);
    InitTouchButton("imgRestart", RestartButtonDown, RestartButtonUp);
    InitTouchButton("imgMenuPlay", MenuPlayDown, MenuPlayUp);
    InitTouchButton("imgMenuOptions", MenuOptionsDown, MenuOptionsUp);
}

function InitTouchButton(handle, touchstart, touchend)
{
    var element = document.getElementById(handle);
    element.addEventListener("touchstart", touchstart, false);
    element.addEventListener("mousedown", touchstart, false);
    element.addEventListener("touchmove", function () { event.preventDefault(); }, false);
    element.addEventListener("touchend", touchend, false);
    element.addEventListener("mouseup", touchend, false);
}

function ConfigureSizes()
{
    var dirButtonWidth = Math.floor($(window).width() / 15);
    var minButtonWidth = 50;
    if (dirButtonWidth < minButtonWidth)
        dirButtonWidth = minButtonWidth;

    var twentyFive = dirButtonWidth / 3;
    var thrityFive = dirButtonWidth * .6;

    $("#imgDown1").width(dirButtonWidth);
    $("#imgDown1").height(dirButtonWidth);
    $("#imgUp1").width(dirButtonWidth);
    $("#imgUp1").height(dirButtonWidth);
    $("#imgLeft").width(dirButtonWidth);
    $("#imgLeft").height(dirButtonWidth);
    $("#imgDown2").width(dirButtonWidth);
    $("#imgDown2").height(dirButtonWidth);
    $("#imgUp2").width(dirButtonWidth);
    $("#imgUp2").height(dirButtonWidth);
    $("#imgRight").width(dirButtonWidth);
    $("#imgRight").height(dirButtonWidth);

    $("#imgDown1")[0].style.left = (dirButtonWidth + 5) + "px";
    $("#imgDown1")[0].style.top = ($(window).height() - dirButtonWidth) + "px";
    $("#imgUp1")[0].style.left = (dirButtonWidth + 5) + "px";
    $("#imgUp1")[0].style.top = ($("#imgDown1").position().top - dirButtonWidth - twentyFive) + "px";
    $("#imgLeft")[0].style.left = "5px";
    $("#imgLeft")[0].style.top = ($("#imgUp1").position().top + dirButtonWidth - twentyFive) + "px";

    $("#imgDown2")[0].style.left = $("#imgDown2")[0].style.left = ($(window).width() - dirButtonWidth * 2 - twentyFive / 8) + "px";
    $("#imgDown2")[0].style.top = ($("#imgDown1").position().top) + "px";
    $("#imgUp2")[0].style.left = ($("#imgDown2").position().left) + "px";
    $("#imgUp2")[0].style.top = ($("#imgUp1").position().top) + "px";
    $("#imgRight")[0].style.left = ($(window).width() - dirButtonWidth - 5) + "px";
    $("#imgRight")[0].style.top = ($("#imgUp2").position().top + dirButtonWidth - twentyFive) + "px";
}

function MenuPlayDown()
{
    $("#imgMenuPlay").css("border", "4px solid #AAAAAA");
    $("#imgMenuPlay").width($("#imgMenuPlay").width() - 6);
    $("#imgMenuPlay").height($("#imgMenuPlay").height() - 6);
    menuPlayDepressed = true;
}

function MenuPlayUp()
{
    if (menuPlayDepressed)
    {
        $("#imgMenuPlay").css("border", "1px solid white");
        $("#imgMenuPlay").width($("#imgMenuPlay").width() + 6);
        $("#imgMenuPlay").height($("#imgMenuPlay").height() + 6);
    }

    InitLevelSelection();
    menuPlayDepressed = false;
}

function MenuOptionsDown()
{
    $("#imgMenuOptions").css("border", "4px solid #AAAAAA");
    $("#imgMenuOptions").width($("#imgMenuOptions").width() - 6);
    $("#imgMenuOptions").height($("#imgMenuOptions").height() - 6);
    menuOptionsDepressed = true;
}

function MenuOptionsUp()
{
    if (menuOptionsDepressed)
    {
        $("#imgMenuOptions").css("border", "1px solid white");
        $("#imgMenuOptions").width($("#imgMenuOptions").width() + 6);
        $("#imgMenuOptions").height($("#imgMenuOptions").height() + 6);
    }

    menuOptionsDepressed = false;
}

function EnableNextLevel()
{
    document.getElementById("imgNextLevel").addEventListener("touchstart", nextLevelButtonDown, false);
    document.getElementById("imgNextLevel").addEventListener("touchend", nextLevelButtonUp, false);
    document.getElementById("imgNextLevel").addEventListener("mouseup", nextLevelButtonUp, false);
    document.getElementById("imgNextLevel").addEventListener("mousedown", nextLevelButtonDown, false);
    nextLevelButtonEnabled = true;
}

function DisableNextLevel()
{
    document.getElementById("imgNextLevel").removeEventListener("touchstart", nextLevelButtonDown, false);
    document.getElementById("imgNextLevel").removeEventListener("touchend", nextLevelButtonUp, false);
    document.getElementById("imgNextLevel").removeEventListener("mouseup", nextLevelButtonUp, false);
    document.getElementById("imgNextLevel").removeEventListener("mousedown", nextLevelButtonDown, false);
    nextLevelButtonEnabled = false;
}

function EnableMenuButton()
{
    document.getElementById("imgMenu").addEventListener("touchstart", MenuButtonDown, false);
    document.getElementById("imgMenu").addEventListener("touchend", MenuButtonUp, false);
    document.getElementById("imgMenu").addEventListener("mouseup", MenuButtonUp, false);
    document.getElementById("imgMenu").addEventListener("mousedown", MenuButtonDown, false);
    menuButtonEnabled = true;
    $("#imgMenu")[0].src = "img/menu.png";
}

function DisableMenuButton()
{
    document.getElementById("imgMenu").removeEventListener("touchstart", MenuButtonDown, false);
    document.getElementById("imgMenu").removeEventListener("touchend", MenuButtonUp, false);
    document.getElementById("imgMenu").removeEventListener("mouseup", MenuButtonUp, false);
    document.getElementById("imgMenu").removeEventListener("mousedown", MenuButtonDown, false);
    menuButtonEnabled = false;
    $("#imgMenu")[0].src = "img/menu_disabled.png";
}


function RestartButtonUp()
{
    if (PreventDoubleClick(1000, $("#imgRestart")))
    {
        clearTimeout(showWinScreenTimeout);
        clearTimeout(openWidthTimeout);
        clearTimeout(getNameTimeOut);
        clearTimeout(showHighScoresTimeout);
        clearTimeout(hideAllTimeout);
        clearTimeout(resumeGameTimeout);
        init = false;
        ReplayLevel();

        $("#imgPause")[0].src = "img/pause.png";
        $("#imgRestart")[0].src = "img/restart.png";
    }
}

function RestartButtonDown()
{
    $("#imgRestart")[0].src = "img/restart2.png";
}

function PauseButtonUp()
{
    if (PreventDoubleClick(3000, $("#imgPause")))
    {
        if (!$("#imgPause").data("paused"))
        {
            $("#imgPause")[0].src = "img/resume_disabled.png";
            clearTimeout(showWinScreenTimeout);
            clearTimeout(openWidthTimeout);
            clearTimeout(getNameTimeOut);
            clearTimeout(showHighScoresTimeout);
            clearTimeout(hideAllTimeout);
            clearTimeout(resumeGameTimeout);
            ShowMenuScreen();
            $("#imgPause").data("paused", true);
        }
        else
        {
            $("#imgPause")[0].src = "img/pause_disabled.png";
            clearTimeout(showWinScreenTimeout);
            clearTimeout(openWidthTimeout);
            clearTimeout(getNameTimeOut);
            clearTimeout(showHighScoresTimeout);
            clearTimeout(hideAllTimeout);
            clearTimeout(resumeGameTimeout);
            HideWinScreen();
            $("#imgPause").data("paused", false);
            resumeGameTimeout = setTimeout(function ()
            {
                Run = GameRun;
                init = true;
                Run();
            }, 3000);
        }
    }
}

function PauseButtonDown()
{
    if ($("#imgPause")[0].src.search("disabled") == -1)
    {
        if ($("#imgPause")[0].src.search("img/pause") > -1)
        {
            $("#imgPause")[0].src = "img/pause2.png";
        }
        else
        {
            $("#imgPause")[0].src = "img/resume2.png";
        }
    }
}

function PlayAgainButtonDown()
{
    $("#imgPlayAgain")[0].src = "img/play_again2.png";
}

function PlayAgainButtonUp()
{
    $("#imgPlayAgain")[0].src = "img/play_again.png";

    if (PreventDoubleClick(5000, $("#imgPlayAgain")))
    {
        init = false;
        ReplayLevel();
    }
}

function PreventDoubleClick(delay, $element)
{
    if ($element.data("preventDoubleClick"))
    {
        return false;
    }

    $element.data("preventDoubleClick", true);
    if ($element[0].src && $element[0].src.indexOf("_disabled") == -1)
    {
        $element[0].src = $element[0].src.replace(".png", "_disabled.png");
    }

    setTimeout(function ()
    {
        $element.data("preventDoubleClick", false);
        if ($element[0].src && $element[0].src.indexOf("_disabled") != -1)
        {
            $element[0].src = $element[0].src.replace("_disabled.png", ".png");
        }
    }, delay);

    return true;
}

var nextLevelButtonDown = function NextLevelButtonDown()
{
    $("#imgNextLevel")[0].src = "img/next_level2.png";
}

var nextLevelButtonUp = function NextLevelButtonUp()
{
    $("#imgNextLevel")[0].src = "img/next_level.png";
    if (PreventDoubleClick(5000, $("#imgNextLevel")))
        NextLevel();
}

function MenuButtonDown()
{
    $("#imgMenu")[0].src = "img/menu2.png";
}

function MenuButtonUp()
{
    DisableMenuButton();
    $("#divMainMenu").show();
    $("#divMainMenuBackground").show();
}

function TouchLeft()
{
    KeyDown({ keyCode: 37 });
}

function TouchUp()
{
    KeyDown({ keyCode: 38 });
}

function TouchDown()
{
    KeyDown({ keyCode: 40 });
}

function TouchRight()
{
    KeyDown({ keyCode: 39 });
}

function TouchLeftEnd()
{
    KeyUp({ keyCode: 37 });
}

function TouchUpEnd()
{
    KeyUp({ keyCode: 38 });
}

function TouchDownEnd()
{
    KeyUp({ keyCode: 40 });
}

function TouchRightEnd()
{
    KeyUp({ keyCode: 39 });
}

function TouchStart(e)
{
    var touch = e.targetTouches[0];
    touchStartX = touch.pageX;
    touchStartY = touch.pageY;
}

function TouchMove(e)
{
    var touch = e.targetTouches[0];
    if (touchTimer < 0)
    {
        if (touch.pageX > touchStartX + maze.shapeWidth)
        {
            horizontalSpeed = horizontalStandardSpeed;
            verticalSpeed = verticalStandardSpeed;
            touchStartX = touch.pageX;
            touchStartY = touch.pageY;
            touchTimer = touchTimerDefault;
        }
        else if (touch.pageX < touchStartX - maze.shapeWidth)
        {
            horizontalSpeed = -horizontalStandardSpeed;
            verticalSpeed = verticalStandardSpeed;
            touchStartX = touch.pageX;
            touchStartY = touch.pageY;
            touchTimer = touchTimerDefault;
        }
        else if (touch.pageY > touchStartY + maze.shapeHeight)
        {
            horizontalSpeed = 0;
            verticalSpeed = horizontalStandardSpeed;
            touchStartX = touch.pageX;
            touchStartY = touch.pageY;
            touchTimer = touchTimerDefault;
        }
        else
        {
            horizontalSpeed = 0;
            verticalSpeed = verticalStandardSpeed;
        }
    }
    touchTimer--;
}

function TouchEnd(e)
{
    mouseMoved = false;
    mouseDown = false;
    horizontalSpeed = 0;
    verticalSpeed = verticalStandardSpeed;
}

function KeyDown(e)
{
    slideLeft = false;
    slideRight = false;
    switch (e.keyCode)
    {
        case 37:
            //Left Key		
            horizontalSpeed = -horizontalStandardSpeed;
            break;
        case 38:
            //Up Key
            Rotate();
            break;
        case 39:
            //Right Key
            horizontalSpeed = horizontalStandardSpeed;
            break;
        case 40:
            //Down Key
            verticalSpeed = horizontalStandardSpeed;
            break;
    }
}

function KeyUp(e)
{
    switch (e.keyCode)
    {
        case 37:
            //Left Key		
            horizontalSpeed = 0;
            break;
        case 38:
            //Up Key

            break;
        case 39:
            //Right Key
            horizontalSpeed = 0;
            break;
        case 40:
            //Down Key
            verticalSpeed = verticalStandardSpeed;
            break;
        case 73:
            //i key
            $("#divImportCustomLevel").show();
            break;
    }
}

function TouchPadMouseMove(e)
{
    lastMouseX = mouseX;
    lastMouseY = mouseY;

    if (typeof e.targetTouches === "undefined")
    {
        mouseX = e.pageX;
        mouseY = e.pageY;
    }
    else
    {
        var touch = e.targetTouches[0];
        mouseX = touch.pageX;
        mouseY = touch.pageY;
    }

    /*if(e.offsetX) 
	{
		mouseX = e.offsetX;
		mouseY = e.offsetY;
	}
	else if(e.layerX) 
	{
		mouseX = e.layerX;
		mouseY = e.layerY;
	}*/

    if (mouseDown)
    {
        var pos = $("#divTouchPad").position();

        $("#divTouchPad").css({
            left: pos.left + (mouseX - lastMouseX),
            top: pos.top + (mouseY - lastMouseY)
        });
    }
}

function TouchPadMouseDown(e)
{
    mouseDown = true;

    if (typeof e.targetTouches === "undefined")
    {
        mouseX = e.pageX;
        mouseY = e.pageY;
    }
    else
    {
        var touch = e.targetTouches[0];
        mouseX = touch.pageX;
        mouseY = touch.pageY;
    }

    lastMouseX = mouseX;
    lastMouseY = mouseY;
}

function TouchPadMouseUp(e)
{
    mouseDown = false;
}

function RunCustomLevel()
{
    level = JSON.parse($("#txtCustomLevel").val());
    $("#divImportCustomLevel").hide();
    NextLevel(true);
}

function CancelCustomLevel()
{
    $("#divImportCustomLevel").hide();
}

function ClearCustomLevel()
{
    $("#txtCustomLevel").val("");
}

function ResetAllButtons()
{
    $("#imgPlayAgain")[0].src = "img/play_again.png";
    $("#imgRestart")[0].src = "img/restart.png";
    $("#imgNextLevel")[0].src = "img/next_level.png";

    if ($("#imgPause")[0].src.search("img/pause") > -1)
    {
        $("#imgPause")[0].src = $("#imgPause")[0].src.replace("pause2", "pause");
    }
    else
    {
        //$("#imgPause")[0].src = "img/resume.png";
        $("#imgPause")[0].src = $("#imgPause")[0].src.replace("resume2", "resume");
    }

    if (menuButtonEnabled)
    {
        $("#imgMenu")[0].src = "img/menu.png";
    }
    else
    {
        $("#imgMenu")[0].src = "img/menu_disabled.png";
    }

    if (nextLevelButtonEnabled)
    {
        $("#imgNextLevel")[0].src = "img/next_level.png";
    }
    else
    {
        $("#imgNextLevel")[0].src = "img/next_level_disabled.png";
    }

    if (menuPlayDepressed)
    {
        $("#imgMenuPlay").css("border", "1px solid white");
        $("#imgMenuPlay").width($("#imgMenuPlay").width() + 6);
        $("#imgMenuPlay").height($("#imgMenuPlay").height() + 6);
    }

    if (menuOptionsDepressed)
    {
        $("#imgMenuOptions").css("border", "1px solid white");
        $("#imgMenuOptions").width($("#imgMenuOptions").width() + 6);
        $("#imgMenuOptions").height($("#imgMenuOptions").height() + 6);
    }

    menuOptionsDepressed = false;
    menuPlayDepressed = false;
}

function InitLevelSelection()
{
    $("#divLevelsMenu").show();
    var str = "<table><tr>";
    for (var i = 0; i < levels.length; i++)
    {
        if (levels[i].unlocked)
        {
            str += "<td class='tdUnlocked' onclick='SelectLevel(" + i + ")' >" + (i + 1) + "</td>";
        }
        else
        {
            str += "<td class='divLockedLevel'>&nbsp;</td>";
        }

        if (i % 5 == 4)
            str += "</tr>";
    }
    str += "</tr></table>";

    var levelHeight = Math.floor($("#divLevelsContainer").height() / 5);
    var fontSize = Math.floor(levelHeight * .45);

    $("#divLevelsMenuHeader")
        .css("font-size", fontSize + "pt")
        .height(fontSize + 5);
    $("#divLevelsMenuHeader img")
        .width(fontSize + 5)
        .height(fontSize + 5);

    $("#divLevelsContainer").html(str);
    $("#divLevelsContainer .tdUnlocked")
        .css("font-size", fontSize + "pt")
        .on("mousedown", function ()
        {
            levelNumberClicked = +$(this).html() - 1;
            var offset = $(this).offset();
            $("#divClickedCell").width($(this).width());
            $("#divClickedCell").height($(this).height());
            $("#divClickedCell").css("left", offset.left);
            $("#divClickedCell").css("top", offset.top);
            $("#divClickedCell").show();
            $("#divClickedCell").on("mouseup", function myfunc()
            {
                $("#divClickedCell").hide();
                $("#divClickedCell").off("mouseup", myfunc);
                SelectLevel(levelNumberClicked);
            });
        });

    var $td = $("#divLevelsContainer td");
    var ratio = $td.width() / $td.height();
    if (ratio > 1)
    {
        $(".divLockedLevel").css("background-size", Math.floor((1 / ratio) * 100) + "% 100%");
    }
    else
    {
        $(".divLockedLevel").css("background-size", "100% " + Math.floor((ratio) * 100) + "%");
    }
}

function SelectLevel(number)
{
    levelNumber = number + 1;
    level = levels[number];

    $("#divLevelsMenu").hide();
    $("#divMainMenu").hide();
    $("#divMainMenuBackground").hide();
    $("#divHighscores").hide();
    $("#divGetName").hide();
    $("#imgPause").show();
    $("#imgRestart").show();
    $("#imgPause")[0].src = "img/pause.png";
    $("#divLevel").html("Level " + levelNumber);

    EnableMenuButton();
    ResetLevel();
    init = false;
    InitLevel(true);
    init = true;
    Run = GameRun;
    $("#divGameWon").css("opacity", 0);
    $("#divGameWon").hide();
    $("#divMenuButtons").css("width", 0);
    $("#divMenuButtons").hide();
    $("#divHighscores").hide();
    Run();
}