﻿var fireWorksAnimation = (function () {
	var fireWorks = [];
	var sparkCount = 100;
	var maxSparkSpeed = 2;
	var sparkSpeedVariance = 0.9;
	var sparkColorVariance = 25;
	var gravity = 0.05;
	var maxSparkSize = 1;
	var fadeSpeed = 5;
	var zoom = 0.5;
	var frequency = 0.09;
	var speed = 25;
	var width = "100%";
	var height = "100%";
	var runTimer;
	var canvas;
	var context;
	var audioPlayers = [];	
	var element;			
	var sounds = ["sound/firework_1.mp3", "sound/firework_2.mp3", "sound/firework_distant_explosion.mp3", "sound/firework_medium_distant_explosion.mp3"];
	var audioCount = 0;
	var bStopping = false;
	var bSound = true;
	var volume = 0.5;
	var sparkCountMax = 0;
	var onStop = undefined;
	var bInit = false;

	function Init(element_in, options)
	{
	    if (element != element_in)
	    {
	        element = element_in;
	        element.innerHTML += "<canvas id='canvasFireworks' style='width: " + width + "; height: " + height + "; background-color: black;' ></canvas>";
	        canvas = document.getElementById("canvasFireworks");
	        context = canvas.getContext("2d");
	    }
		
		Update(options);		

		if (!bInit)
		{
		    element.innerHTML += "<canvas id='canvasFireworks' style='width: " + width + "; height: " + height + "; background-color: black;' ></canvas>";
		    canvas = document.getElementById("canvasFireworks");
		    context = canvas.getContext("2d");
		    canvas.width = canvas.offsetWidth * zoom;
		    canvas.height = canvas.offsetHeight * zoom;

		    //Preload 20 sound effects for 20 simultaneous sounds
		    for (var j = 0; j < 5; j++)
		    {
		        for (var i = 0; i < sounds.length; i++)
		        {
		            CreateAudioPlayer(sounds[i]);
		        }
		    }

		    bInit = true;
		}
	}
	
	function Update(options)
	{
		if(options)
		{
			if(options.width !== undefined)
				width = options.width;						
			if(options.height !== undefined)
				height = options.height;
			if(options.sparkCount !== undefined)
				sparkCount = options.sparkCount;
			if(options.maxSparkSpeed !== undefined)
				maxSparkSpeed = options.maxSparkSpeed;
			if(options.sparkSpeedVariance !== undefined)
				sparkSpeedVariance = options.sparkSpeedVariance;
			if(options.sparkColorVariance !== undefined)
				sparkColorVariance = options.sparkColorVariance;
			if(options.gravity !== undefined)
				gravity = options.gravity;
			if(options.maxSparkSpeed !== undefined)
				maxSparkSpeed = options.maxSparkSpeed;
			if(options.fadeSpeed !== undefined)
				fadeSpeed = options.fadeSpeed;
			if(options.zoom !== undefined)
				zoom = options.zoom;
			if(options.frequency !== undefined)
				frequency = options.frequency;
			if(options.speed !== undefined)
				speed = options.speed;
			if(options.sound !== undefined)
			    bSound = options.sound;
			if (options.volume !== undefined)            
			    volume = options.volume;
		}
	}
	
	function CreateAudioPlayer(soundFile)
	{
		var audioId = "ap_" + audioCount++;
		var audioSrc = soundFile;
		var audioElement = document.createElement('audio');
		audioElement.id = audioId;
		audioElement.setAttribute('src', audioSrc);
		audioElement.setAttribute('preload', 'auto');
		element.appendChild(audioElement);
		audioPlayers.push(audioElement);
		
		return audioId;				
	}
	
	function CreateFirework()
	{
		//var index = Math.floor(Math.random() * 4);
		//console.log(index);
		//audioPlayers[index].play();
		
		var brightness = 0;
		var color;
		while(brightness < 256)
		{
			color = {
				r: Math.floor(Math.random() * 256),
				g: Math.floor(Math.random() * 256),
				b: Math.floor(Math.random() * 256)
			};
			brightness = color.r + color.g + color.b;
		}
		
		var newFirework = {
			x: Math.floor(Math.random() * (canvas.width * .9) + canvas.width * .1),
			y: Math.floor(Math.random() * (canvas.height * .5) + canvas.height * .1),
			baseColor: color,
			sparks: []
		};
		
		for(var j = 0; j < sparkCount; j++)
		{
			var a = Math.random() * (Math.PI * 2);
			var variance = maxSparkSpeed * sparkSpeedVariance;						
			var velocity = Math.random() * variance + (maxSparkSpeed - variance);
			var sparkColor = {
				r: Math.floor(Math.random() * sparkColorVariance) + newFirework.baseColor.r,
				g: Math.floor(Math.random() * sparkColorVariance) + newFirework.baseColor.g,
				b: Math.floor(Math.random() * sparkColorVariance) + newFirework.baseColor.b
			};
			if(sparkColor.r > 255)
				sparkColor.r = 255;
			if(sparkColor.g > 255)
				sparkColor.g = 255;
			if(sparkColor.b > 255)
				sparkColor.b = 255;
				
			var sparkSize =  Math.random() * maxSparkSize + maxSparkSize / 2;
			var velocity = velocity * 0.9;
			var	spark = {
				x: newFirework.x,
				y: newFirework.y,
				mx: Math.cos(a) * velocity,
				my: Math.sin(a) * velocity,
				a: a,
				color: sparkColor,
				size: Math.floor(sparkSize),
				length: 1,
				velocity: velocity
			};
				
			
			
			newFirework.sparks.push(spark);			
		}
		fireWorks.push(newFirework);					
	}
	
	function CreateFireworkWithSound()
	{
		var cnt = 0;
		while(cnt < 20)
		{
			var audioPlayer = audioPlayers[Math.floor(Math.random() * audioPlayers.length)];
			if(audioPlayer.paused || audioPlayer.ended)
			{
			    audioPlayer.volume = volume;
				audioPlayer.play();
				CreateFirework();
				cnt = 20;			
			}
			cnt++;
		}
	}
	
	function Animate()
	{				
		var sparkCnt = 0;
		var removeFireworks = [];
		for(var i = 0; i < fireWorks.length; i++)
		{
			var fireWork = fireWorks[i];
			var removeSparks = [];
			sparkCnt += fireWork.sparks.length;
			for(var j = 0; j < fireWork.sparks.length; j++)
			{
				var spark = fireWork.sparks[j];
				spark.my += gravity;
				spark.x += spark.mx;
				spark.y += spark.my;
				spark.color.r -= fadeSpeed;
				spark.color.g -= fadeSpeed;
				spark.color.b -= fadeSpeed;
				spark.length += spark.velocity / 3;
				
				if(spark.color.r <= 0)
					spark.color.r = 0;
				if(spark.color.g <= 0)
					spark.color.g = 0;
				if(spark.color.b <= 0)
					spark.color.b = 0;
				
				if(spark.x < 0 || spark.y < 0 || spark.x > canvas.width || spark.y > canvas.height || (spark.color.r == 0 && spark.color.g == 0 && spark.color.b == 0))
				{
					removeSparks.push(j);
				}
			}
			for(var j = removeSparks.length - 1; j >= 0; j--)
			{
				fireWork.sparks.splice(removeSparks[j], 1);
			}
			
			if(fireWork.sparks.length == 0)
			{
				removeFireworks.push(i);				
			}
		}
		
		for(var i = removeFireworks.length - 1; i >= 0; i--)
		{
			fireWorks.splice(removeFireworks[i], 1);
		}
		if(sparkCnt > sparkCountMax)
			sparkCountMax = sparkCnt;
	}
		
	function Draw()
	{
		context.clearRect(0, 0, canvas.width, canvas.height);
		for(var i = 0; i < fireWorks.length; i++)
		{
			for(var j = 0; j < fireWorks[i].sparks.length; j++)
			{
				var spark = fireWorks[i].sparks[j];
				var strColor = "rgb(" + spark.color.r + ", " + spark.color.g + ", " + spark.color.b + ")";
				context.save();
				context.fillStyle = strColor;
				context.beginPath();
				context.moveTo(spark.x, spark.y);
				context.lineTo(spark.x - Math.cos(spark.a) * spark.length, spark.y - Math.sin(spark.a) * spark.length);
				context.lineWidth = Math.floor(spark.size);
				context.strokeStyle = strColor;
				context.lineCap = "round";
				context.stroke();	
				context.restore();
			}
		}
	}
	
	function Run()
	{		
		Animate();
		Draw();
		runTimer = setTimeout(function () {
			Run();
		}, speed);
		
		if(!bStopping && Math.random() < frequency)
		{
			if(bSound)
				CreateFireworkWithSound();
			else
				CreateFirework();
		}
		
		if (bStopping && fireWorks.length == 0)
		{
		    clearTimeout(runTimer);
		    onStop();
		}			
	}
	
	function Stop(onStop_in)
	{
	    onStop = onStop_in;
		bStopping = true;
	}

	function Start()
	{
	    bStopping = false;
	    Run();
	}
	
	var publicAPI = {
		Init: Init,
		Start: Start,
		Stop: Stop,
		Update: Update
	};
	
	return publicAPI;
})();