﻿var canvasActive;
var canvasNextParts;
var canvasBackground;
var canvasBackgroundShadow;
var canvasActiveShadow

var ctxActive;
var ctxNextParts;
var ctxBackground;
var ctxBackgroundShadow;
var ctxActiveShadow;
var ctxGrid;

var maze = {
	blockWidth: 20,
	blockHeight: 20,
	blockColor: "red",
	shapes: [],
	shapeWidth: 60,
	shapeHeight: 60,
	blocks: [],
	grid: [],
	width: 0,
	height: 0
};


var verticalSpeed = 0;
var horizontalSpeed = 0;
var verticalStandardSpeed;
var horizontalStandardSpeed;

var init = false;

var active = {};
var nextParts = [];

var divStats;
var startBlock;
var finishBlock;
var path = [];
var pathGradient;
var pathGradientNormal;
var pathGradientWin;
var pathGradientZero;
var nextPartsGradient;
var startGradient;
var endGradient;
var startColor2;
var endColor2;
var slideGradient;

var level;
var levelNumber = 1;

var dt;
var lastTime;
var shadowSize;
var nextPartsSize;
var GraphicsRes = "high";
var DrawBlockFunction;
var DrawPathFunction;
var Run = GameRun;
var finalScore = 0;
var finalRanking = 0;
var getNameTimeOut = 0;
var fontSize;
var fontSize2;
var fontSize3;
var imgWidth;
var menuWidth;
var openWidthTimeout = 0;
var showWinScreenTimeout = 0;
var showHighScoresTimeout = 0;
var hideAllTimeout = 0;
var slideLeft = false;
var slideRight = false;

var musicVolume = .5;
var volumeTicks = 30;
var audioPlayer;
var audioPlayerTimeout;
var audioTrack = 0;
var musicOn = true;

var musicLoaded = false;

window.onready = function () {
	var loadingScreen;

	setTimeout( function () {
		loadingScreen = document.getElementById( "divLoadingScreen" );
		loadingScreen.style.opacity = 0;
		setTimeout( function () {
			document.body.removeChild(
				loadingScreen
			);
		}, 1000 );
	}, 1500 );
}

function Init()
{
    if (localStorage)
    {
        if (localStorage.GraphicsRes)
            GraphicsRes = localStorage.GraphicsRes;

        if (localStorage.buttonSize)
            buttonSize = parseFloat(localStorage.buttonSize);

        if (localStorage.musicVolume && localStorage.musicVolume != "undefined")
            musicVolume = parseFloat(localStorage.musicVolume);

        if (localStorage.keypadStyle)
            keypadStyle = localStorage.keypadStyle;

        if (localStorage.lastChapter)        
            chapter = parseInt(localStorage.lastChapter);

        if (localStorage.musicOn)
        {
            if (localStorage.musicOn == "true")
                musicOn = true;
            else
                musicOn = false;
        }            
    }

    InitMusic();
    InitFireworks();

	divStats = document.getElementById("divStats");
	canvasActive = document.getElementById("myCanvasActive");
	canvasNextParts = document.getElementById("myCanvasNextParts");
	canvasBackground = document.getElementById("myCanvasBackground");
	canvasBackgroundShadow = document.getElementById("myCanvasBackgroundShadow");
	canvasActiveShadow = document.getElementById("myCanvasActiveShadow");
	
	ResetCanvasSize();
	
	ctxActive = canvasActive.getContext("2d");
	ctxNextParts = canvasNextParts.getContext("2d");
	ctxBackground = canvasBackground.getContext("2d");
	ctxBackgroundShadow = canvasBackgroundShadow.getContext("2d");
	ctxActiveShadow = canvasActiveShadow.getContext("2d");
		
	ctxBackground.clearRect(0, 0, canvasBackground.width, canvasBackground.height);
	
	InitTouchPad();	
	level = levels[levelNumber - 1];

	GetUnlockedLevels();

	var fontSizeMin = 10;
	var minOptionsButtonWidth = 60;
	var optionsButtonWidth = $(window).width() / 10;

	if (optionsButtonWidth < minOptionsButtonWidth)
	    optionsButtonWidth = minOptionsButtonWidth;

	fontSize = Math.floor($(window).width() / 60);
	fontSize2 = Math.floor(fontSize * 1.35);
	fontSize3 = Math.floor(fontSize * .65);

	if (fontSize < fontSizeMin)
	    fontSize = fontSizeMin;
	if (fontSize2 < fontSizeMin)
	    fontSize2 = fontSizeMin;
	if (fontSize3 < fontSizeMin)
	    fontSize3 = fontSizeMin;

	var mainMenuHeight = $(window).height() * .6;
	
	if (mainMenuHeight < 300)
	{
	    mainMenuHeight = $(window).height() * .9;	
	}

	var mainMenuWidth = mainMenuHeight * 1.1667;

	$("#divMainMenu").height(mainMenuHeight);
	$("#divMainMenu").width(mainMenuWidth);
	$("#divMainMenu").css("left", Math.floor(($(window).width() - mainMenuWidth) / 2) + "px");
	$("#divMainMenu").css("top", Math.floor(($(window).height() - mainMenuHeight) / 2) + "px");

	setTimeout(function ()
	{
	    var menuButtonsTop = Math.floor(($(window).height() - $("#divMenuButtons").height()) / 2);
	    $("#divMenuButtons").css("top", menuButtonsTop + "px");
	    if($("#imgPause").offset().top + $("#imgPause").height() - 10 > menuButtonsTop)
	    {
	        $("#imgPause").css("left", $(window).width() - $("#imgPause").width() - 10);
	        $("#imgPause").css("top", 10);
	    }
        
	},
    100);
	    
	imgWidth = Math.floor($(window).width() / 16);
	menuWidth = optionsButtonWidth * 1.2;

	$(".imgLevelClearedButtons").width(optionsButtonWidth);
	$("#imgLevelCleared").width(Math.floor($(window).width() * .45));

	$("#divGameWon div").css("font-size", fontSize + "pt");
	$("#spanTotalScoreLabel").css("font-size", fontSize2 + "pt");
	$("#spanMinScoreLabel").css("font-size", fontSize2 + "pt");
	$("#divMinScore").css("font-size", fontSize2 + "pt");
	$("#divGameWon table img").width(imgWidth);
	$("#divGameWon").css("font-size", fontSize + "pt");

	$(".ScoreLabel").css("top", Math.floor(imgWidth / 3 * -1));
	InitLevel(true);
	
	init = true;
	
	var d = new Date();
	lastTime = d.getTime();
	//setTimeout(Run, 10);
	MenuButtonUp();
}

function ResetCanvasSize()
{
    var maxNextpartsWidth = 150;
    var nextPartsWidth = $("#divNextParts").width()
    if (nextPartsWidth > maxNextpartsWidth)
        nextPartsWidth = maxNextpartsWidth;

    $("#divNextParts").width(nextPartsWidth);

    $(canvasActive).width($(window).width() - nextPartsWidth - 50);
    $(canvasActive).height($(window).height() - 15);

    canvasActive.width = canvasActive.offsetWidth;
    canvasActive.height = canvasActive.offsetHeight;
    canvasNextParts.width = canvasNextParts.offsetWidth;
    canvasNextParts.height = canvasNextParts.offsetHeight;

    canvasBackground.offsetWidth = canvasActive.offsetWidth;
    canvasBackground.offsetHeight = canvasActive.offsetHeight;
    canvasBackground.width = canvasActive.offsetWidth;
    canvasBackground.height = canvasActive.offsetHeight;
}

function ReplayLevel()
{
	ResetLevel();
	InitLevel(true);
	Run = GameRun;
	PlayMusic();
	HideWinScreen(Run);
}

function NextLevel(customLevel)
{
    ResetCanvasSize();
    DisableNextLevel();
    if (!customLevel)
    {
        levelNumber++;
        level = levels[levelNumber - 1];
    }
    $("#divLevel").html("Level " + levelNumber);
    ResetLevel();
    InitLevel(true);
    Run = GameRun;
    PlayMusic();
    HideWinScreen(Run);
}

function ResetLevel()
{
    ResetCanvasSize();
	maze.blocks = [];
	maze.grid = [];
	maze.shapes = [];
	nextParts = [];
}

function HideWinScreen(fnContinue)
{
    clearTimeout(showWinScreenTimeout);
    clearTimeout(openWidthTimeout);
    clearTimeout(getNameTimeOut);
    clearTimeout(showHighScoresTimeout);
    clearTimeout(hideAllTimeout);

    $("#divGameWon")
        .css("animation", "fadeout 1s")
        .css("-webkit-animation", "fadeout 1s");

    hideAllTimeout = setTimeout(function ()
    {
	    $("#divGameWon").css("opacity", 0);
	    $("#divGameWon").hide();
	    $("#divMenuButtons").css("width", 0);
	    $("#divMenuButtons").hide();
	    $("#divHighscores").hide();	    
	    fnContinue();
    }, 900);

	$("#divMenuButtons")
        .css("animation", "closeoptions 1s")
        .css("-webkit-animation", "closeoptions 1s");
	   
	$("#divHighscores")
        .css("animation", "CloseHighScores 1s")
        .css("-webkit-animation", "CloseHighScores 1s");

	$("#divGetName").hide();

	$("#divMainMenu").hide();
	$("#divMainMenuBackground").hide();
	$("#imgPause").show();
	//$("#imgRestart").show();

    //if(!$("#imgPause").data("disabled"))
	    $("#imgPause")[0].src = "img/pause.png";
    //if (!$("#imgRestart").data("disabled"))
     //  $("#imgRestart")[0].src = "img/restart.png";

	EnableMenuButton();
}

function ShowMenuButtons()
{
    clearTimeout(showWinScreenTimeout);
    clearTimeout(openWidthTimeout);
    clearTimeout(getNameTimeOut);
    clearTimeout(showHighScoresTimeout);
    clearTimeout(hideAllTimeout);

    $("#divMenuButtons")
        .css("animation", "closeoptions 3s")
        .css("-webkit-animation", "closeoptions 3s");
    $("#imgPause")[0].src = "img/resume.png";    
}

function UpdateCanvasLayer(canvas)
{
	canvas.width = canvasActive.width;
	canvas.height = canvasActive.height;
	canvas.style.width = canvasActive.style.width;
	canvas.style.height = canvasActive.style.height;
	canvas.style.left =  ($("#myCanvasActive").position().left) + "px";
	canvas.style.top =  ($("#myCanvasActive").position().top) + "px";
}

function InitLevel(isLevelChanging)
{
    ResetCanvasSize();

	maze.blockWidth = Math.floor( canvasActive.width / ( level.blockCountWidth * level.maze[0].length ) );
	maze.blockHeight = Math.floor( canvasActive.height / ( level.blockCountHeight * level.maze.length ) );
	
	if(maze.blockWidth > maze.blockHeight)
		maze.blockWidth = maze.blockHeight;
	else
		maze.blockHeight = maze.blockWidth;
	
	shadowSize = maze.blockWidth / 4;
	
	verticalStandardSpeed = level.verticalStandardSpeed * maze.blockWidth;
	horizontalStandardSpeed = level.horizontalStandardSpeed * maze.blockWidth;
	
	verticalSpeed = verticalStandardSpeed;
	
	maze.shapeWidth = maze.blockWidth * level.blockCountWidth;
	maze.shapeHeight = maze.blockHeight * level.blockCountHeight;
	maze.width = maze.shapeWidth * level.maze[0].length;
	maze.height = maze.shapeHeight * level.maze.length;

	if(isLevelChanging)
	{
        //Set the active canvas size
		canvasActive.width = maze.shapeWidth * level.maze[0].length;
		canvasActive.height = maze.shapeHeight * level.maze.length;			
		canvasActive.style.width = canvasActive.width + "px";
		canvasActive.style.height = canvasActive.height + "px";
		
        //Get the size of the next parts
		var nextPartsBlockWidth = Math.floor($("#divNextParts").width() / 3);
		var nextPartsBlockHeight = Math.floor((canvasActive.height - 30) / (5 * 4.25));
		if (nextPartsBlockWidth > nextPartsBlockHeight)
		    nextPartsSize = nextPartsBlockHeight;
		else
		    nextPartsSize = nextPartsBlockWidth;

        //Set the size of the next parts window
		var divNextPartsWidth = (nextPartsSize + 6) * 3;
		$("#divNextParts").width(divNextPartsWidth);

        //Set the active canvas position
		var canvasActiveLeft = ($(window).width() / 2 - ($(canvasActive).width() + divNextPartsWidth) / 2);
		
		$(canvasActive)[0].style.left = canvasActiveLeft + "px";
		$("#divNextParts")[0].style.left = (canvasActiveLeft + canvasActive.width + 10) + "px";		

		$(canvasActive)[0].style.top = Math.floor(($(window).height() - $(canvasActive).height()) / 2) + "px";
		$("#divNextParts")[0].style.top = $(canvasActive).position().top + "px";
		$("#divNextParts").height(canvasActive.height);

		UpdateCanvasLayer(canvasBackground);
		UpdateCanvasLayer(canvasBackgroundShadow);
		UpdateCanvasLayer(canvasActiveShadow);
		
		canvasNextParts.height = (canvasActive.height - 30);
		canvasNextParts.style.height = canvasNextParts.height + "px";
		canvasNextParts.width = divNextPartsWidth;
		canvasNextParts.style.width = canvasNextParts.width + "px";
		
		canvasNextParts.style.left = ($(canvasActive).position().left + $(canvasActive).width()) + "px";
	}
	
	if(GraphicsRes == "high")
	{
		InitColorsHighRes();
		DrawBlockFunction = DrawBlockHighRes;
		DrawPathFunction = DrawPathHighRes;
		ctxGrid = ctxBackgroundShadow;

		$(canvasBackgroundShadow).show();
		$(canvasActiveShadow).show();
		$(canvasBackground).css("background-color", "rgba(0, 0, 0, 0)");
	}
	else if(GraphicsRes = "low")
	{
		InitColorsLowRes();
		DrawBlockFunction = DrawBlockLowRes;
		DrawPathFunction = DrawPathLowRes;
		ctxGrid = ctxBackground;
		shadowSize = 0;
		$(canvasBackgroundShadow).hide();
		$(canvasActiveShadow).hide();
		$(canvasBackground).css("background-color", "black");
	}
		
	InitMaze();

	startBlock = {
	    x: level.startShape.x * maze.shapeWidth + maze.blockWidth,
	    y: level.startShape.y * maze.shapeHeight + maze.blockHeight
	};
	
	finishBlock = {
	    x: level.finishShape.x * maze.shapeWidth + maze.blockWidth,
	    y: level.finishShape.y * maze.shapeHeight + maze.blockHeight
	};
	

	for(var i = 0; i < level.maze.length; i++)
	{
		for(var j = 0; j < level.maze[i].length; j++)
		{
			var x = j * maze.shapeWidth;
			var y = i * maze.shapeHeight;
			
			var val = level.maze[i][j];
			if(val > 0)
			{
			    var shape = shapes[val];
			    if (level.rotations[j + "X" + i] != undefined)
			    {
			        for (var k = 0; k < level.rotations[j + "X" + i]; k++)
			            shape = RotateShape(shape);
			    }
			    SetBlock(x, y, CopyShape(shape));
			}
		}
	}		
	
	for (var i = 0; i < 5; i++)
	{
	    var nextPartIndex = Math.floor(Math.random() * level.shapes.length);
	    if ($.inArray(nextPartIndex, level.excludedShapes) != -1)
	    {
	        i--;
	    }
	    else
	    {
	        var nextPart = CopyShape(level.shapes[nextPartIndex]);
	        nextParts.push(nextPart);
	    }	    
	}
	DetectWin();
	SortBlocks(maze.blocks);	
	Draw();
	
	init = true;
	GetNextShape();
}

function InitColorsHighRes()
{
	level.gradients = [];
	for(var i = 0; i < level.colors.length; i++)
	{
		var gradient = ctxBackground.createLinearGradient(0, 0, maze.blockWidth, maze.blockHeight);
		gradient.addColorStop(0, level.colors2[i]);
		gradient.addColorStop(1, level.colors[i]);
		level.gradients[i] = gradient;
	}
	
	level.gradientsDistinct = [];
	for(var i = 0; i < level.colorsDistinct.length; i++)
	{
		var gradient = ctxNextParts.createLinearGradient(0, 0, maze.blockWidth, maze.blockHeight);
		gradient.addColorStop(0, level.colorsDistinct2[i]);
		gradient.addColorStop(1, level.colorsDistinct[i]);
		level.gradientsDistinct[i] = gradient;
	}
	
	active.gradient = ctxActive.createLinearGradient(0, 0, maze.blockWidth, maze.blockHeight);	
	active.gradient.addColorStop(0, "#d8bfd8");
	active.gradient.addColorStop(1, "#662266");
	active.color2 = "#662266";
	
	slideGradient = ctxActive.createLinearGradient(0, 0, maze.shapeWidth * .5, maze.shapeHeight * .5);
	slideGradient.addColorStop(0, "#d8bfd8");
	slideGradient.addColorStop(1, "#551155");

	var nextPartsGradientWidth = Math.floor(canvasNextParts.width / 3);
	var nextPartsGradientHeight = Math.floor(canvasNextParts.height / (5 * 4.25));

	if (nextPartsGradientWidth > nextPartsGradientHeight)
	    nextPartsGradientWidth = nextPartsGradientHeight;
	else
	    nextPartsGradientHeight = nextPartsGradientWidth;

	nextPartsGradient = ctxNextParts.createLinearGradient(0, 0, nextPartsGradientWidth, nextPartsGradientHeight);
	nextPartsGradient.addColorStop(0, "#d8bfd8");
	nextPartsGradient.addColorStop(1, "#662266");	

	pathGradientNormal = ctxBackground.createLinearGradient(0, 0, maze.blockWidth, maze.blockHeight);
	pathGradientNormal.addColorStop(0, "#d8bfd8");
	pathGradientNormal.addColorStop(1, "#662266");
	
	pathGradientWin = ctxBackground.createLinearGradient(0, 0, maze.blockWidth, maze.blockHeight);
	pathGradientWin.addColorStop(0, "#FFFF00");
	pathGradientWin.addColorStop(1, "#555500");
	
	pathGradientZero = ctxBackground.createLinearGradient(0, 0, maze.blockWidth, maze.blockHeight);
	pathGradientZero.addColorStop(0, "#888888");
	pathGradientZero.addColorStop(1, "#000000");
	
	pathGradient = pathGradientNormal;
	
	startGradient = ctxBackground.createLinearGradient(0, 0, maze.blockWidth, maze.blockHeight);
	startGradient.addColorStop(0, "#BBBBBB");
	startGradient.addColorStop(1, "#555555");
	startColor2 = "#555555";
	
	endGradient = ctxBackground.createLinearGradient(0, 0, maze.blockWidth, maze.blockHeight);
	endGradient.addColorStop(0, "#BBBBBB");
	endGradient.addColorStop(1, "#555555");
	endColor2 = "#555555";	
}

function InitColorsLowRes()
{
	level.gradients = [];
	for(var i = 0; i < level.colors.length; i++)
	{
		level.gradients[i] = level.colors2[i];
	}
	
	level.gradientsDistinct = [];
	for(var i = 0; i < level.colorsDistinct.length; i++)
	{
		level.gradientsDistinct[i] = level.colorsDistinct2[i];
	}
	
	//active.gradient = "#d8bfd8";
	active.gradient = "#995599";
	slideGradient = "#995599";

	pathGradientNormal = "#995599";
	pathGradientWin = "#FFFF00";
	pathGradientZero = "#777777";
	
	pathGradient = pathGradientNormal;
	
	startGradient = "#BBBBBB";
	startColor2 = "#555555";
	endGradient = "#BBBBBB";
	endColor2 = "#555555";

	nextPartsGradient = active.gradient;
}

function InitMaze()
{
	for(var x = 0; x < level.maze[0].length * level.blockCountWidth; x++)
	{
		maze.grid[x] = [];
			
		for(var y = 0; y < level.maze.length * level.blockCountHeight; y++)
		{
			maze.grid[x][y] = -1;
		}
	}
}

function GetShapeColor(x)
{
	var section = Math.floor( x / maze.shapeWidth);
	
	var index = Math.floor(section / level.maze[0].length * level.colors.length);
	
	return { 
		color1: level.gradients[index],
		color2: level.colors[index]
	};
	
	//return level.colors[Math.floor(Math.random() * level.colors.length)];
}

function GetNextShape()
{
    var nextPartIndex = Math.floor(Math.random() * level.shapes.length);
    while ($.inArray(nextPartIndex, level.excludedShapes) != -1)
        nextPartIndex = Math.floor(Math.random() * level.shapes.length);

    var nextPart = CopyShape(level.shapes[nextPartIndex]);
	
	var rotates = Math.floor(Math.random() * 10);
	for(var i = 0; i < rotates; i++)
		nextPart = RotateShape(nextPart);
	
	nextParts.push(nextPart);
	active.x = Math.floor((canvasActive.width / 2) / maze.shapeWidth) * maze.shapeWidth;
	active.y = -maze.shapeHeight;
	active.shape = nextParts[0];
	nextParts.splice(0, 1);
	
	DrawNextParts();
}

function Rotate()
{
	active.shape = RotateShape(active.shape);
}

function DetectMazeCollision(x, y)
{
	var collideX = false;
	var collideY = false;
	
	for(var i = 0; i < maze.shapes.length; i++)
	{
		if(x + maze.shapeWidth > maze.shapes[i].grid.x && x < maze.shapes[i].grid.x + maze.shapeWidth)
		{
			if(y + maze.shapeHeight > maze.shapes[i].grid.y && y < maze.shapes[i].grid.y + maze.shapeHeight)
			{
				return true;
			}
		}
	}
	
	return false;
}

function GetGridCoordinates(x, y, shape)
{
	var gridX = Math.round(x / (maze.blockWidth * shape[0].length)) * (maze.blockWidth * shape[0].length);
	var gridY = Math.round(y / (maze.blockHeight * shape.length)) * (maze.blockHeight * shape.length); 
	
	return { x: gridX, y: gridY };
}

function GetFloorCoordinates(x, y, shape)
{
    var gridX = Math.floor(x / (maze.blockWidth * shape[0].length)) * (maze.blockWidth * shape[0].length);
    var gridY = Math.floor(y / (maze.blockHeight * shape.length)) * (maze.blockHeight * shape.length);

    return { x: gridX, y: gridY };
}

function SortBlocks(blocks)
{
	for( var i = 0; i < blocks.length; i++)
	{
		var value = blocks[i];
		for(var j = i - 1; j > -1 && blocks[j].y * 10 + blocks[j].x > value.y * 10 + value.x; j--)
		{
			blocks[j + 1] = blocks[j];
		}
		
		blocks[j + 1] = value;
	}
}

function SetBlock(x, y, shape, color, color2)
{
    verticalSpeed = verticalStandardSpeed;
    slideLeft = false;
    slideRight = false;
	if(!color)
	{
		colors = GetShapeColor(x);
		color = colors.color1;
		color2 = colors.color2;
	}
	
	var grid = GetGridCoordinates(x, y, shape);
	var i = 0;
	var j = 0;
	for(var x2 = 0; x2 < maze.shapeWidth; x2 += maze.blockWidth)
	{
		for(var y2 = 0; y2 < maze.shapeHeight; y2 += maze.blockHeight)
		{
			var mazeX = (grid.x + x2) / maze.blockWidth;
			var mazeY = (grid.y + y2) / maze.blockHeight;
			
			if(shape[i][j] == 1)
			{
				maze.blocks.push({
					x: grid.x + x2, 
					y: grid.y + y2,
					color: color,
					color2: color2,
					path: false
				});
				maze.grid[mazeX][mazeY] = 1;
			}
			else
			{
				maze.grid[mazeX][mazeY] = 0;
			}
			j++;
		}
		i++;
		j = 0;
	}
		
	maze.shapes.push({
		grid: grid
	});
	
	var bWin = false;
	if(init)
	{
	    bWin = DetectWin();	
	    if (y < -maze.blockHeight)
		{
		    finalScore = 0;
		    $("#spanScore").html(0);

		    $("#divMinScore").html(level.minimumScore);
		    $("#divWinPathScore").html(0 + " * 3 = " + 0);
		    $("#divDeadEndScore").html(0 + " * 2 = " + 0);
		    $("#divPenalty").html(0 + " * 3 = " + 0);
		    $("#divTotalScore").html("<span style='color: gold'>" + 0 + "</span> + <span style='color: gold'>" + 0 +
                "</span> - <span style='color: #EC0825'>" + 0 + "</span> = <span style='color: white;'>" + 0 + "</span>");

		    Run = function () { };
		    PauseMusic();

		    //$("#divGameWon").show();
		    bWin = true;
		    $("#imgNextLevel")[0].src = "img/next_level_disabled.png";
		    DisableNextLevel();
		    if ($("#imgLevelCleared")[0].src.search("img/game_over.png") != -1)
		    {
		        ShowWinScreen();
		    }
		    else
		    {
		        $("#imgLevelCleared")[0].src = "img/game_over.png";
		        $("#imgLevelCleared")[0].onload = ShowWinScreen;
		    }
		}
		active.y = -500;
	}	
	
	SortBlocks(maze.blocks);	
	Draw();
	
	return !bWin;
}

function PauseMusic()
{
    if (window.HTMLAudioElement)
    {
        try
        {
            if (audioPlayer && !audioPlayer.paused)
            {
                audioPlayer.pause();
                clearTimeout(audioPlayerTimeout);
            }                
        }
        catch(e)
        {}
    }
}

function PlayMusic()
{
    if (window.HTMLAudioElement)
    {
        try
        {
            if (musicOn)
            {
                audioPlayer.volume = musicVolume;
                if (audioPlayer.paused)
                    audioPlayer.play();

                setTimeout(function ()
                {
                    var timeLeft = audioPlayer.duration - audioPlayer.currentTime;

                    if (!isNaN(timeLeft))
                    {
                        clearTimeout(audioPlayerTimeout);
                        audioPlayerTimeout = setTimeout(function ()
                        {
                            if (Run == GameRun)
                            {
                                audioPlayer.pause();
                                SelectMusic();
                                PlayMusic();
                            }
                        }, (timeLeft * 1000) + 1000);
                    }
                    else
                    {
                        clearTimeout(audioPlayerTimeout);
                        audioPlayerTimeout = setTimeout(function ()
                        {
                            PlayMusic();
                        }, 500);
                    }
                }, 5000);
            }
            else
            {
                if (!audioPlayer.paused)
                    audioPlayer.pause();
            }
        }
        catch(e)
        {
            console.log(e);
        }
    }
}

function InitMusic()
{
    var $audio = $("audio");
    audioTrack = Math.floor(Math.random() * $audio.length);
    SelectMusic();
}

function SelectMusic()
{
    var $audio = $("audio");
    if(audioPlayer)
        audioPlayer.pause();
    audioPlayer = $audio[audioTrack];
    
    audioTrack++;
    if (audioTrack >= $audio.length)
        audioTrack = 0;
}

function GameRun()
{
	var d = new Date();
	var t = d.getTime();
	dt = (t - lastTime) / 30;
	lastTime = t;
	if(dt * horizontalStandardSpeed > (maze.blockWidth / 2))
		dt = (maze.blockWidth / 2) / horizontalStandardSpeed;
	
	MoveBlocks();	
	ctxActive.clearRect(0, 0, canvasActive.width, canvasActive.height);
	ctxActiveShadow.clearRect(0, 0, canvasActiveShadow.width, canvasActiveShadow.height);
	DrawShape(active.shape, active.x, active.y, active.gradient, active.color2, ctxActive, false, ctxActiveShadow);
	DrawSlide();
	//ctxActive.fillStyle = active.color2;
	//ctxActive.fillRect(active.x + maze.shapeWidth, active.y + maze.shapeHeight, maze.shapeWidth, maze.shapeWidth);
	//ctxActive.fillRect(active.x - maze.shapeWidth, active.y + maze.shapeHeight, maze.shapeWidth, maze.shapeWidth);
    //active.x + maze.shapeWidth, active.y + maze.shapeHeight
    //active.x - maze.shapeWidth, active.y + maze.shapeHeight
	DisplayFPS();
	setTimeout(Run, 30);
	mouseMoving = false;
}

function DrawSlide()
{
    if (slideRight)
    {
        ctxActive.save();
        ctxActive.translate(active.x + maze.shapeWidth * 1.2, active.y);
        ctxActive.beginPath();
        ctxActive.moveTo(0, maze.shapeHeight * .3);
        ctxActive.lineTo(maze.shapeWidth * 0.3, maze.shapeHeight * .5);
        ctxActive.lineTo(0, maze.shapeHeight * .7);
        ctxActive.lineTo(maze.shapeWidth * 0.1, maze.shapeHeight * .5);
        ctxActive.lineTo(0, maze.shapeHeight * .3);
        ctxActive.fillStyle = slideGradient;
        ctxActive.fill();
        ctxActive.restore();

        ctxActive.save();
        ctxActive.translate(active.x + maze.shapeWidth, active.y);
        ctxActive.beginPath();
        ctxActive.moveTo(0, maze.shapeHeight * .3);
        ctxActive.lineTo(maze.shapeWidth * 0.3, maze.shapeHeight * .5);
        ctxActive.lineTo(0, maze.shapeHeight * .7);
        ctxActive.lineTo(maze.shapeWidth * 0.1, maze.shapeHeight * .5);
        ctxActive.lineTo(0, maze.shapeHeight * .3);
        ctxActive.fillStyle = slideGradient;
        ctxActive.fill();
        ctxActive.restore();
    }
    else if (slideLeft)
    {
        ctxActive.save();
        ctxActive.translate(active.x, active.y);
        ctxActive.beginPath();
        ctxActive.moveTo(0, maze.shapeHeight * .3);
        ctxActive.lineTo(-maze.shapeWidth * 0.3, maze.shapeHeight * .5);
        ctxActive.lineTo(0, maze.shapeHeight * .7);
        ctxActive.lineTo(-maze.shapeWidth * 0.1, maze.shapeHeight * .5);
        ctxActive.lineTo(0, maze.shapeHeight * .3);
        ctxActive.fillStyle = slideGradient;
        ctxActive.fill();
        ctxActive.restore();

        ctxActive.save();
        ctxActive.translate(active.x - maze.shapeWidth * 0.2, active.y);
        ctxActive.beginPath();
        ctxActive.moveTo(0, maze.shapeHeight * .3);
        ctxActive.lineTo(-maze.shapeWidth * 0.3, maze.shapeHeight * .5);
        ctxActive.lineTo(0, maze.shapeHeight * .7);
        ctxActive.lineTo(-maze.shapeWidth * 0.1, maze.shapeHeight * .5);
        ctxActive.lineTo(0, maze.shapeHeight * .3);
        ctxActive.fillStyle = slideGradient;
        ctxActive.fill();
        ctxActive.restore();       
    }
}

function MoveBlocks()
{
	var hSpeed = horizontalSpeed * dt;
	var vSpeed = verticalSpeed * dt;
	
	var grid = GetGridCoordinates(active.x, active.y + vSpeed, active.shape);
	var floor = GetFloorCoordinates(active.x, active.y + vSpeed, active.shape);

	if (slideRight)
	{
	    if (floor.y > active.y && !DetectMazeCollision(floor.x + maze.shapeWidth, floor.y))
	    {
	        active.x = floor.x + maze.shapeWidth;
	        active.y = floor.y;
	        slideRight = false;
	    }
	}

	if (slideLeft)
	{
	    if (floor.y > active.y && !DetectMazeCollision(floor.x - maze.shapeWidth, floor.y))
	    {
	        active.x = floor.x - maze.shapeWidth;
	        active.y = floor.y;
	        slideLeft = false;
	    }
	}
	if(grid.x == active.x)
	{
		if(DetectMazeCollision(active.x, active.y + vSpeed))
		{
			if( SetBlock(active.x, active.y, active.shape) )
				GetNextShape();
		}
	}
	else
	{		
		if(DetectMazeCollision(grid.x, grid.y))
		{
			horizontalSpeed = 0;	
			hSpeed = 0;
			if( SetBlock(active.x, active.y, active.shape))
			{
				GetNextShape();
			}
		}
	}
	
	if(active.x <= 0 - horizontalSpeed)
	{
	    horizontalSpeed = 0;
	    hSpeed = 0;
	    active.x = 0;
	}
	else if (active.x > canvasActive.width - maze.shapeWidth - horizontalSpeed)
	{
	    horizontalSpeed = 0;
	    hSpeed = 0;
	    active.x = canvasActive.width - maze.shapeWidth;
	}
	else if(DetectMazeCollision(active.x + hSpeed, active.y))
	{
	    if (hSpeed > 0)
	    {
	        if (!DetectMazeCollision(grid.x + maze.shapeWidth, floor.y + maze.shapeHeight))
	        {
	            slideRight = true;
	        }
	    }
	    else if (hSpeed < 0)
	    {
	        if (!DetectMazeCollision(grid.x - maze.shapeWidth, floor.y + maze.shapeHeight))
	        {
	            slideLeft = true;
	        }
	    }
		horizontalSpeed = 0;	
		hSpeed = 0;        
	}
    		
	if(active.y >= canvasActive.height - maze.shapeHeight || DetectMazeCollision())
	{
		if( SetBlock(active.x, active.y, active.shape) )
			GetNextShape();
	}
	else
	{
		active.y += vSpeed;		
	}
	
	active.x += hSpeed;
	
	//Slide to grid
	if(horizontalSpeed == 0)
	{
		var grid = GetGridCoordinates(active.x, active.y, active.shape);
		if(active.x != grid.x)
		{
			if(active.x + horizontalStandardSpeed * dt < grid.x)
			{
				active.x += horizontalStandardSpeed * dt;
			}
			else if(active.x - horizontalStandardSpeed * dt > grid.x)
			{
				active.x -= horizontalStandardSpeed * dt;
			}
			else
			{
				active.x = grid.x;
			}
		}
	}
}

function Draw()
{        
	ctxBackground.clearRect(0, 0, canvasActive.width, canvasActive.height);
	ctxBackgroundShadow.clearRect(0, 0, canvasActive.width, canvasActive.height);
	
	DrawGrid(ctxGrid);
	
	for(var i = 0; i < maze.blocks.length; i++)
	{
		var block = maze.blocks[i];
		var index = 0;
		if(block.path)
			DrawPathFunction( block.x, block.y, block.color, ctxBackground, ctxBackgroundShadow);
		else
			DrawBlockFunction(block.x, block.y, block.color, block.color2, ctxBackground, ctxBackgroundShadow);
	}

	ctxBackground.fillStyle = "White";
	ctxBackground.font = Math.floor(fontSize) + "px Verdana";

	var metrics = ctxBackground.measureText("START");
	ctxBackground.fillText("START",
        level.startShape.x * maze.shapeWidth + (maze.shapeWidth - metrics.width) / 2,
        level.startShape.y * maze.shapeHeight + maze.shapeHeight / 2 + 5 + shadowSize / 2);

	metrics = ctxBackground.measureText("FINISH");
	ctxBackground.fillText("FINISH",
        level.finishShape.x * maze.shapeWidth + (maze.shapeWidth - metrics.width) / 2,
        level.finishShape.y * maze.shapeHeight + maze.shapeHeight / 2 + 5 + shadowSize / 2);

}

function DrawGrid(ctx)
{
    var cnt = 0;
    for (var x = shadowSize; x < canvasActive.width; x += maze.blockWidth)
    {
        ctx.beginPath();
        if (cnt < maze.shapeWidth)
        {
            ctx.strokeStyle = "#003388";
            cnt += maze.blockWidth;
        }
        else
        {
            ctx.strokeStyle = "#5588FF";
            cnt = maze.blockWidth;
        }
        ctx.moveTo(x, shadowSize);
        ctx.lineTo(x, canvasActive.height);
        ctx.stroke();
    }

    cnt = 0;
    for (var y = shadowSize; y < canvasActive.width; y += maze.blockHeight)
    {
        ctx.beginPath();
        if (cnt < maze.shapeHeight)
        {
            ctx.strokeStyle = "#003388";
            cnt += maze.blockHeight;
        }
        else
        {
            ctx.strokeStyle = "#5588FF";
            cnt = maze.blockHeight;
        }
        ctx.moveTo(shadowSize, y);
        ctx.lineTo(canvasActive.width, y);
        ctx.stroke();
    }
}

function DrawNextParts()
{
	ctxNextParts.clearRect(0, 0, canvasNextParts.width, canvasNextParts.height);
	
	var tempBlockWidth = maze.blockWidth;
	var tempBlockHeight = maze.blockHeight;
	maze.blockWidth = Math.floor(canvasNextParts.width / 3);
	maze.blockHeight = Math.floor(canvasNextParts.height / (nextParts.length * 4.25));
	if (maze.blockWidth > maze.blockHeight)	
	    maze.blockWidth = maze.blockHeight;
	else
	    maze.blockHeight = maze.blockWidth;

	var tempShadowSize = shadowSize;
	shadowSize = maze.blockWidth / 4;

	var tempShapeWidth = maze.shapeWidth;
	var tempShapeHeight = maze.shapeHeight;
	maze.shapeWidth = maze.blockWidth * 3;
	maze.shapeHeight = maze.blockHeight * 3;

	for(var i = 0; i < nextParts.length; i++)
	{		
	    DrawShape(nextParts[i], 5, 5 + i * (maze.shapeHeight + shadowSize + 15), nextPartsGradient, active.color2, ctxNextParts, true, ctxNextParts);
	    //DrawBlockFunction(0, i * (maze.shapeHeight + 30), level.colors2[level.colors2.length - 1], level.gradients[level.gradients.length - 1], ctxNextParts, ctxNextParts);
	}

	maze.blockWidth = tempBlockWidth;
	maze.blockHeight = tempBlockHeight;
	maze.shapeWidth = tempShapeWidth;
	maze.shapeHeight = tempShapeHeight;
	shadowSize = tempShadowSize;
}

function DrawShape(shape, x, y, color, color2, ctx, border, ctxShadow)
{
	var i = 0;
	var j = 0;
	
	if(border)
	{
	    ctx.save();
		ctx.beginPath();
		ctx.endCap = "line";
		ctx.lineWidth = 3;
		ctx.strokeStyle = color2;
		ctx.rect(x - 5, y - 5, maze.shapeWidth + shadowSize + 10, maze.shapeHeight + shadowSize + 10);
		ctx.stroke();
		ctx.restore();
	}
		
	for(var x2 = 0; x2 < maze.shapeWidth; x2 += maze.blockWidth)
	{
		for(var y2 = 0; y2 < maze.shapeHeight; y2 += maze.blockHeight)
		{
			var block = shape[i][j];
			if(block == 1)
			{			    
				DrawBlockFunction(x + x2, y + y2, color, color2, ctx, ctxShadow);
			}
			j++;			
		}
		i++;
		j = 0;
	}	
}

function DrawBlockHighRes(x, y, color, color2, ctx, ctxShadow)
{	
	ctxShadow.save();
				
	ctxShadow.strokeStyle = color2;
	ctxShadow.translate(x, y);
	ctxShadow.beginPath();
	ctxShadow.moveTo(maze.blockWidth, 0);
	ctxShadow.lineTo(maze.blockWidth + shadowSize, shadowSize);
	ctxShadow.lineTo(maze.blockWidth + shadowSize, maze.blockHeight + shadowSize);				
	ctxShadow.lineTo(shadowSize, maze.blockHeight + shadowSize);
	ctxShadow.lineTo(0, maze.blockHeight);				
	ctxShadow.fillStyle = color;
	ctxShadow.fill();
	ctxShadow.stroke();
	
	ctxShadow.beginPath();
	ctxShadow.strokeStyle = color2;
	ctxShadow.moveTo(maze.blockWidth, maze.blockHeight);
	ctxShadow.lineTo(maze.blockWidth + shadowSize, maze.blockHeight + shadowSize);
	ctxShadow.stroke();				
	ctxShadow.restore();
	
	ctx.save();
	ctx.beginPath();
	ctx.strokeStyle = color2;
	ctx.translate(x, y - 1);
	ctx.fillStyle = color;
	ctx.fillRect(0, 0, maze.blockWidth, maze.blockHeight);
	ctx.rect(0, 0, maze.blockWidth, maze.blockHeight);
	ctx.stroke();
	ctx.restore();
}

function DrawBlockLowRes(x, y, color, color2, ctx)
{
	ctx.save();
	ctx.beginPath();
	ctx.strokeStyle = color2;
	ctx.translate(x, y - 1);
	ctx.fillStyle = color;
	ctx.fillRect(0, 0, maze.blockWidth, maze.blockHeight + 1);
	ctx.rect(0, 0, maze.blockWidth, maze.blockHeight + 1);
	ctx.stroke();
	ctx.restore();
}

function DrawPathHighRes(x, y, color, ctx, ctxShadow)
{
	ctxShadow.save();
	ctxShadow.translate(x + shadowSize, y + shadowSize);
	ctxShadow.fillStyle = color;
	ctxShadow.fillRect(0, 0, maze.blockWidth, maze.blockHeight);
	ctxShadow.restore();	
}

function DrawPathLowRes(x, y, color, ctx, ctxShadow)
{
	ctx.save();
	ctx.translate(x, y);
	ctx.fillStyle = color;
	ctx.fillRect(0, 0, maze.blockWidth, maze.blockHeight);
	ctx.restore();	
}

function DetectWin()
{
	var start = {
		x: startBlock.x / maze.blockWidth,
		y: startBlock.y / maze.blockHeight,
		parent: 0
	};
	var finish = {
		x: finishBlock.x / maze.blockWidth,
		y: finishBlock.y / maze.blockHeight
	};
	
	var routes = [start];
	var lastRoute = {};
	var bWin = false;
	var deadEnds = [];
	var winRoute = false;
	
	while(routes.length > 0)
	{
		var newRoutes = [];
		for(var i = 0; i < routes.length; i++)
		{
			var route = routes[i];	
			var bDeadEnd = true;
			if(route.x == finish.x && route.y == finish.y)
			{
				lastRoute = route;
				winRoute = route;
				bWin = true;
				bDeadEnd = false;				
			}
			maze.grid[route.x][route.y] = 2;
			
			//Move Left
			if(route.x > 0 && maze.grid[route.x - 1][route.y] == 0)
			{				
				newRoutes.push({x: route.x - 1, y: route.y, parent: route});
				bDeadEnd = false;
			}
			
			//Move Right
			if(route.x < maze.grid.length - 1 && maze.grid[route.x + 1][route.y] == 0)
			{
				newRoutes.push({x: route.x + 1, y: route.y, parent: route});
				bDeadEnd = false;
			}
			
			//Move Up
			if(route.y > 0 && maze.grid[route.x][route.y - 1] == 0)
			{
				newRoutes.push({x: route.x, y: route.y - 1, parent: route});
				bDeadEnd = false;
			}
			
			//Move Down
			if(route.y < maze.grid[route.x].length - 1 && maze.grid[route.x][route.y + 1] == 0)
			{
				newRoutes.push({x: route.x, y: route.y + 1, parent: route});
				bDeadEnd = false;
			}
			
			lastRoute = route;
			
			if(bDeadEnd)
			{
				deadEnds.push(route);
			}
		}
		
		routes = newRoutes;
	}
	
	path = [];
	RefreshBlocks();
	if(bWin)
	{
		Draw();
		var winBlocks = [];
		var deadEndBlocks = [];
		var noPathBlocks = [];
		
		while(winRoute.parent != 0)
		{
			maze.grid[winRoute.x][winRoute.y] = 3;
			path.push(winRoute);
			winRoute = winRoute.parent;			
		}
		maze.grid[winRoute.x][winRoute.y] = 3;
		path.push(winRoute);
		
		//Mark all the dead end paths
		for(var i = 0; i < deadEnds.length; i++)
		{
			var deadEndPath = deadEnds[i];
			while(deadEndPath.parent != 0)
			{
				if(maze.grid[deadEndPath.x][deadEndPath.y] != 3)
					maze.grid[deadEndPath.x][deadEndPath.y] = 4;
				
				deadEndPath = deadEndPath.parent;
			}
		}

		//Count the dead end blocks and draw the maze		
		var deadEndBlockCount = 0;
		var noPathCount = 0;
		for(var i = 0; i < maze.grid.length; i++)
		{
			for(var j = 0; j < maze.grid.length; j++)
			{
				if(maze.grid[i][j] == 4)
				{
					deadEndBlockCount++;
					maze.blocks.push({
						x: i * maze.blockWidth, 
						y: j * maze.blockHeight,
						color: pathGradientNormal,
						path: true
					});					
				}
				else if(maze.grid[i][j] == 3)
				{
					maze.blocks.push({
						x: i * maze.blockWidth, 
						y: j * maze.blockHeight,
						color: pathGradientWin,
						path: true
					});
				}
				else if(maze.grid[i][j] == 0)
				{
					noPathCount++;					
					maze.blocks.push({
						x: i * maze.blockWidth, 
						y: j * maze.blockHeight,
						color: pathGradientZero,
						path: true
					});
				}				
			}
		}
		
		pathGradient = pathGradientWin;
		Run = function () {};
		PauseMusic();

		var pathScore = path.length * 3;
		var deadEndScore = deadEndBlockCount * 2;
		var noPathPenalty = noPathCount * 3;
		finalScore = pathScore + deadEndScore - noPathPenalty;
		$("#spanScore").html(finalScore);

		$("#divMinScore").html(level.minimumScore);
		$("#divWinPathScore").html(path.length + " * 3 = " + pathScore);
		$("#divDeadEndScore").html(deadEndBlockCount + " * 2 = " + deadEndScore);
		$("#divPenalty").html(noPathCount + " * 3 = " + noPathPenalty);
		$("#divTotalScore").html("<span style='color: gold'>" + pathScore + "</span> + <span style='color: gold'>" + deadEndScore +
            "</span> - <span style='color: #EC0825'>" + noPathPenalty + "</span> = <span style='color: white;'>" + finalScore + "</span>");

		$("#imgLevelCleared").css("opacity", 1);

		if (finalScore >= level.minimumScore)
		{
		    $("#imgNextLevel")[0].src = "img/next_level.png";
            if(levelNumber < levels.length)
                EnableNextLevel();

		    UnlockNextLevel();
		   
		    if ($("#imgLevelCleared")[0].src.search("img/level_cleared.png") != -1)
		    {
		        ShowWinScreen();
		    }
		    else
		    {
		        $("#imgLevelCleared")[0].src = "img/level_cleared.png";
		        $("#imgLevelCleared")[0].onload = ShowWinScreen;
		    }
		}
		else
		{
		    $("#imgNextLevel")[0].src = "img/next_level_disabled.png";
		    DisableNextLevel();
		    if ($("#imgLevelCleared")[0].src.search("img/game_over.png") != -1)
		    {
		        ShowWinScreen();
		    }
		    else
		    {
		        $("#imgLevelCleared")[0].src = "img/game_over.png";
		        $("#imgLevelCleared")[0].onload = ShowWinScreen;
		    }		    
		}
	}
	else
	{		
		while(lastRoute.parent != 0)
		{
			maze.blocks.push({
				x: lastRoute.x * maze.blockWidth, 
				y: lastRoute.y * maze.blockHeight,
				color: pathGradient,
				path: true
			});
			path.push(lastRoute);
			lastRoute = lastRoute.parent;			
		}
		maze.blocks.push({
			x: lastRoute.x * maze.blockWidth, 
			y: lastRoute.y * maze.blockHeight,
			color: pathGradient,
			path: true
		});
		path.push(lastRoute);
		
		for(var i = 0; i < maze.grid.length; i++)
		{
			for(var j = 0; j < maze.grid.length; j++)
			{
				if(maze.grid[i][j] == 2)
					maze.grid[i][j] = 0;
			}
		}
	}

	return bWin;
}

function GetUnlockedLevels()
{
    try
    {
        if (localStorage && localStorage.unlockedLevels)
        {
            var unlockedLevels = JSON.parse(localStorage.unlockedLevels);
            for(var i = 0; i < unlockedLevels.length; i++)
            {
                levels[unlockedLevels[i]].unlocked = true;
            }
        }
    }
    catch(e)
    {

    }
}

function UnlockNextLevel()
{
    if (levelNumber < levels.length)
    {
        levels[levelNumber].unlocked = true;
        if(levelNumber % 15 == 0)
            ShowFireworks("Chapter Cleared", false);
    }
    else
    {
        ShowFireworks("You've Won The Game", true);
    }

    try
    {
        if (localStorage && localStorage.unlockedLevels)
        {
            var unlockedLevels = JSON.parse(localStorage.unlockedLevels);
            var bFound = false;
            for (var i = 0; i < unlockedLevels.length; i++)
            {
                if (unlockedLevels[i] == levelNumber)
                    bFound = true;
            }
            if(!bFound)
                unlockedLevels.push(levelNumber);
            localStorage.unlockedLevels = JSON.stringify(unlockedLevels);
        }    
        else if(localStorage)
        {
            localStorage.unlockedLevels = JSON.stringify([levelNumber]);
        }
    }
    catch(e)
    {
        
    }
}

function ShowMenuScreen()
{
    Run = function () { };
    PauseMusic();
    $("#imgNextLevel")[0].src = "img/next_level_disabled.png";
    DisableNextLevel();
   
    $("#divMenuButtons div").css("font-size", fontSize + "pt");
    $("#divMenuButtons")
        .show()
        .css("animation", "openoptions 1s")
        .css("-webkit-animation", "openoptions 1s");
    openWidthTimeout = setTimeout(function () { $("#divMenuButtons").css("width", "15%"); }, 900);    
}

function ShowWinScreen()
{
    init = false;

	$("#divGameWon").show();

	$("#divGameWon").css("animation", "fadein 5s");
	$("#divGameWon").css("-webkit-animation", "fadein 5s");
    showWinScreenTimeout = setTimeout(function () { $("#divGameWon").css("opacity", 1) }, 4900);
	
	$("#divMenuButtons div").css("font-size", fontSize + "pt");
	$("#divMenuButtons")
        .show()
        .css("animation", "openoptions 2s")
        .css("-webkit-animation", "openoptions 2s");
    openWidthTimeout = setTimeout(function () { $("#divMenuButtons").css("width", "15%"); }, 1900);

    EnableMenuButton();

	getRankingAttempts = 0;

    if(finalScore >= level.minimumScore) {
        //GetRanking();
	}

    $("#imgPause").hide();
    //$("#imgRestart").hide();
	//var str = CreateMazeString();
	//var grids = CreateMazeFromString(str);	
}

var getRankingAttempts;
function GetRanking()
{
    //Get the score ranking
    $.ajax({
        url: "http://dev.fountainapps.com/games/maze/bus/process_high_scores.php?Action=GetRanking",
        method: "POST",
        //contentType: "application/json; charset=utf-8",
        data: {
            Score: finalScore,
            Level: levelNumber
        },
        error: function (data)
        {
            
            if (getRankingAttempts < 5)
            {
                getRankingAttempts++;
                setTimeout(GetRanking, 500);
            }
        },
        success: function (ranking)
        {
            finalRanking = parseInt(ranking);

            if (finalRanking < 100000)
            {
                finalRanking++;
                $("#spanRanking").html(ConvertToRankingPosition(finalRanking));
                getTopTenAttempts = 0;

                if($(window).width() > 500) {
					//GetTopTen();
				}
                
                getNameTimeOut = setTimeout(function ()
                {
                    $("#divGetName").show();
                    $("#divGetName").css("left", Math.floor(($(window).width() - $("#divGetName").width()) / 2));
                    $("#divGetName").css("top", Math.floor(($(window).height() - $("#divGetName").height()) / 2));
                }, 5000);
            }
        }
    });
}

var getTopTenAttempts;
function GetTopTen()
{
    $.ajax({
        url: "http://dev.fountainapps.com/games/maze/bus/process_high_scores.php?Action=GetTop10",
        method: "POST",
        //contentType: "application/json; charset=utf-8",
        data: {
            Score: finalScore,
            Level: levelNumber,
            Ranking: finalRanking
        },
        error: function() {
            if(getTopTenAttempts < 5)
            {
                getTopTenAttempts++;
                getTopTenTimeOut = setTimeout(GetTopTen, 500);
            }
        },
        success: function (data)
        {
            $("#ulTop10").html(data);
            var oldWidth = fontSize3 * 20;

            $("#divHighscores li").width(oldWidth);
            $("#divHighscores li").css("text-align", "left");
            $("#divHighscores")
                .css("font-size", (fontSize3) + "pt")
                .css("left", $(window).width() - 5)
                .width(0)
                .css("top", $("#divMenuButtons").position().top)
                .show()
                .css("animation", "OpenHighScores 2s")
                .css("animation", "OpenHighScores 2s");

            showHighScoresTimeout = setTimeout(function ()
            {
                $("#divHighscores").css("width", "20%");
                $("#divHighscores").css("left", "79%");
            }, 1900);
        }        
    });
}

function ConvertToRankingPosition(n) 
{
   var s = ["th","st","nd","rd"],
	   v = n%100;
   return n+(s[(v-20)%10]||s[v]||s[0]);
}

function RefreshBlocks()
{
	var removeBlocks = [];
	for(var i = 0; i < maze.blocks.length; i++)
	{
		if(maze.blocks[i].path)
			removeBlocks.push(i);
	}
	for(var i = removeBlocks.length - 1; i >= 0; i--)
	{
		maze.blocks.splice( removeBlocks[i], 1 );
	}
}

function CancelSubmitScore()
{
	$("#divGetName").hide();
}

var submitScoreAttempts = 0;
function SubmitScore()
{
	var name = $("#txtName").val();
	if(name.length < 3 || name.length > 12)
	{
		//alert("Name must be more than 2 characters.");
		return;
	}
	var garbled = "uR_M1RQ!EnIK(yugIaJk2#mBNm$4vE_5@#6C";
	var moreGarbled = CryptoJS.MD5(name + levelNumber + finalScore + garbled);
    
	//alert(finalScore);
	$.ajax({
	    url: "http://dev.fountainapps.com/games/maze/bus/process_high_scores.php?Action=SubmitScore",
		method: "POST",
		//contentType: "application/json; charset=utf-8",
		data: {
			Level: levelNumber,
			Score: finalScore,
			Name: name,
			Stuff: moreGarbled.toString(CryptoJS.enc.Base64)
		},
		success: function (data) {			
			if(data)
			{
				//console.log(data);
			    //alert(data);			
			    $("#spanYou").html(data);
			}			
		},
		error: function (msg) {
			//alert(msg);
		    //console.log(msg);
		    if (submitScoreAttempts < 5)
		    {
		        submitScoreAttempts++;
		        setTimeout(SubmitScore, 500);
		    }		    
		}
	});	
	$("#divGetName").hide();
}

function CreateMazeString()
{
	var bits = 0;
	var val = 0;
	var str = String.fromCharCode(maze.grid.length) + String.fromCharCode(maze.grid[0].length);
	
	for(var i = 0; i < maze.grid.length; i++)
	{
		for(var j = 0; j < maze.grid[i].length; j++)
		{
			if(maze.grid[i][j] == 1)
				val += Math.pow(2, bits);
			bits++;
			if(bits >= 8)
			{
				bits = 0;				
				str += String.fromCharCode(val);
				val = 0;
			}
		}
		bits = 0;		
		str += String.fromCharCode(val);
		val = 0;
	}
	
	return str;
}

function CreateMazeFromString(str)
{
	for(var i = 0; i < str.length; i++)
	{
		var val = str.charCodeAt(i);		
	}
}

function ConvertFromChar(chr)
{
	var bits = [];
		
	for(var j = 7; j >= 0; j--)
	{
		bits[7-i] = Math.floor( val / Math.pow(2, i));
		val -= bits[7-i];
	}			
}