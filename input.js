﻿
var touchTimer = 0;
var touchTimerDefault = 10;
var touchStartX;
var touchStartY;
var touchX;
var touchY;

var mouseX;
var mouseY;
var lastMouseX;
var lastMouseY;
var mouseDown;
var divTouchPad;
//var preventDoubleClick = false;
var menuPlayDepressed = false;
var menuOptionsDepressed = false;
var menuHelpDepressed = false;

var nextLevelButtonEnabled = false;
var menuButtonEnabled = true;
var resumeGameTimeout = -1;
var levelNumberClicked = -1;

var buttonSizeSmall = .75;
var buttonSizeMedium = 1;
var buttonSizeLarge = 1.5;
var buttonSize = buttonSizeMedium;

var keypadStyle = "two hands";

var chapter = 0;
var chapterSize = 15;

var zoomlevel = 1;

function InitTouchPad()
{
    $("body").dblclick(function (ev)
    {        
        $(this).css({
            "-moz-transform": "scale(" + zoomlevel + ")",
            "-webkit-transform": "scale(" + zoomlevel + ")",
            "-o-transform": "scale(" + zoomlevel + ")",
            "-ms-transform": "scale(" + zoomlevel + ")"
        });
    });

    $("img").each(function ()
    {
        $(this)[0].ondragstart = function (e) { e.preventDefault(); };
        $(this)[0].ondrop = function (e) { e.preventDefault(); };
    });

    document.onselectstart = function (e)
    {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
    };
    document.onContextMenu = function (e)
    {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
    };
    document.oncontextmenu = function (e)
    {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
    };

    ConfigureSizes();

    InitTouchButton("imgLeft", TouchLeft, TouchLeftEnd);
    InitTouchButton("imgUp1", TouchUp, TouchUpEnd);
    InitTouchButton("imgUp2", TouchUp, TouchUpEnd);
    InitTouchButton("imgRight", TouchRight, TouchRightEnd);
    InitTouchButton("imgDown1", TouchDown, TouchDownEnd);
    InitTouchButton("imgDown2", TouchDown, TouchDownEnd);
    InitTouchButton("imgPlayAgain", PlayAgainButtonDown, PlayAgainButtonUp);
    InitTouchButton("imgNextLevel", nextLevelButtonDown, nextLevelButtonUp);
    InitTouchButton("imgMenu", MenuButtonDown, MenuButtonUp);
    InitTouchButton("imgPause", PauseButtonDown, PauseButtonUp);
    InitTouchButton("imgMenuPlay", MenuPlayDown, MenuPlayUp);
    InitTouchButton("imgMenuOptions", MenuOptionsDown, MenuOptionsUp);
    InitTouchButton("imgMenuHelp", MenuHelpDown, MenuHelpUp);
}

function InitTouchButton(handle, touchstart, touchend)
{
    var element = document.getElementById(handle);
    element.addEventListener("touchstart", touchstart, false);
    element.addEventListener("mousedown", touchstart, false);
    element.addEventListener("touchmove", function (e)
    {
        e.preventDefault();
    }, false);
    element.addEventListener("touchend", touchend, false);
    element.addEventListener("mouseup", touchend, false);
}

function GetSize(sizeFactor)
{
    var dirButtonWidth = Math.floor($(window).width() / 15) * sizeFactor;
    var minButtonWidth = 50;
    if (dirButtonWidth < minButtonWidth)
        dirButtonWidth = minButtonWidth;

    return dirButtonWidth;
}

function ConfigureSizes()
{
    var dirButtonWidth = GetSize(buttonSize);

    $("#imgDown1").width(dirButtonWidth);
    $("#imgDown1").height(dirButtonWidth);
    $("#imgUp1").width(dirButtonWidth);
    $("#imgUp1").height(dirButtonWidth);
    $("#imgLeft").width(dirButtonWidth);
    $("#imgLeft").height(dirButtonWidth);
    $("#imgDown2").width(dirButtonWidth);
    $("#imgDown2").height(dirButtonWidth);
    $("#imgUp2").width(dirButtonWidth);
    $("#imgUp2").height(dirButtonWidth);
    $("#imgRight").width(dirButtonWidth);
    $("#imgRight").height(dirButtonWidth);

    var twentyFive = dirButtonWidth / 3;
    var thrityFive = dirButtonWidth * .6;

    if (keypadStyle == "two hands")
    {
        $("#imgDown2").show();
        $("#imgUp2").show();
        $("#imgRight").show();
        $("#imgDown1").show();
        $("#imgUp1").show();
        $("#imgLeft").show();

        $("#imgDown1")[0].style.left = (dirButtonWidth + 5) + "px";
        $("#imgDown1")[0].style.top = ($(window).height() - dirButtonWidth) + "px";
        $("#imgUp1")[0].style.left = (dirButtonWidth + 5) + "px";
        $("#imgUp1")[0].style.top = ($("#imgDown1").position().top - dirButtonWidth - twentyFive) + "px";
        $("#imgLeft")[0].style.left = "5px";
        $("#imgLeft")[0].style.top = ($("#imgUp1").position().top + dirButtonWidth - twentyFive) + "px";

        $("#imgDown2")[0].style.left = ($(window).width() - dirButtonWidth * 2 - twentyFive / 8) + "px";
        $("#imgDown2")[0].style.top = ($("#imgDown1").position().top) + "px";
        $("#imgUp2")[0].style.left = ($("#imgDown2").position().left) + "px";
        $("#imgUp2")[0].style.top = ($("#imgUp1").position().top) + "px";
        $("#imgRight")[0].style.left = ($(window).width() - dirButtonWidth - 5) + "px";
        $("#imgRight")[0].style.top = ($("#imgUp2").position().top + dirButtonWidth - twentyFive) + "px";
    }
    else if (keypadStyle == "left hand")
    {
        $("#imgDown2").hide();
        $("#imgUp2").hide();
        $("#imgRight").show();
        $("#imgDown1").show();
        $("#imgUp1").show();
        $("#imgLeft").show();

        $("#imgDown1")[0].style.left = (dirButtonWidth + 5) + "px";
        $("#imgDown1")[0].style.top = ($(window).height() - dirButtonWidth) + "px";
        $("#imgUp1")[0].style.left = (dirButtonWidth + 5) + "px";
        $("#imgUp1")[0].style.top = ($("#imgDown1").position().top - dirButtonWidth - twentyFive) + "px";
        $("#imgLeft")[0].style.left = "5px";
        $("#imgLeft")[0].style.top = ($("#imgUp1").position().top + dirButtonWidth - twentyFive) + "px";
        $("#imgRight")[0].style.left = ($("#imgUp1").position().left + dirButtonWidth - 5) + "px";
        $("#imgRight")[0].style.top = ($("#imgUp1").position().top + dirButtonWidth - twentyFive) + "px";
    }
    else if(keypadStyle == "right hand")
    {
        $("#imgDown1").hide();
        $("#imgUp1").hide();
        $("#imgRight").show();
        $("#imgDown2").show();
        $("#imgUp2").show();
        $("#imgLeft").show();

        $("#imgDown2")[0].style.left = ($(window).width() - dirButtonWidth * 2 - twentyFive / 8) + "px";
        $("#imgDown2")[0].style.top = ($(window).height() - dirButtonWidth) + "px";
        $("#imgUp2")[0].style.left = ($("#imgDown2").position().left) + "px";
        $("#imgUp2")[0].style.top = ($("#imgDown2").position().top - dirButtonWidth - twentyFive) + "px";
        $("#imgLeft")[0].style.left = ($("#imgUp2").position().left - dirButtonWidth - 5) + "px";
        $("#imgLeft")[0].style.top = ($("#imgUp2").position().top + dirButtonWidth - twentyFive) + "px";
        $("#imgRight")[0].style.left = ($(window).width() - dirButtonWidth - 5) + "px";
        $("#imgRight")[0].style.top = ($("#imgUp2").position().top + dirButtonWidth - twentyFive) + "px";
    }
}

var menuPlaySizeOriginal = false;
function MenuPlayDown()
{
    if (!menuPlaySizeOriginal)
    {
        menuPlaySizeOriginal = {
            width: $("#imgMenuPlay").width(),
            height: $("#imgMenuPlay").height()
        };
    }

    if ($("#imgMenuPlay").width() == menuPlaySizeOriginal.width && $("#imgMenuPlay").height() == menuPlaySizeOriginal.height)
    {
        $("#imgMenuPlay").css("border", "4px solid #AAAAAA");
        $("#imgMenuPlay").width($("#imgMenuPlay").width() - 6);
        $("#imgMenuPlay").height($("#imgMenuPlay").height() - 6);
    }
    
    menuPlayDepressed = true;
}

function MenuPlayUp()
{
    if (menuPlayDepressed)
    {
        $("#imgMenuPlay").css("border", "1px solid white");
        $("#imgMenuPlay").width(menuPlaySizeOriginal.width);
        $("#imgMenuPlay").height(menuPlaySizeOriginal.height);
        setTimeout(function ()
        {
            InitLevelSelection();
        }, 500);
    }
    menuPlayDepressed = false;
}

function MenuOptionsDown()
{
    $("#imgMenuOptions").css("border", "4px solid #AAAAAA");
    $("#imgMenuOptions").width($("#imgMenuOptions").width() - 6);
    $("#imgMenuOptions").height($("#imgMenuOptions").height() - 6);
    menuOptionsDepressed = true;
}

function MenuOptionsUp()
{
    if (menuOptionsDepressed)
    {
        $("#imgMenuOptions").css("border", "1px solid white");
        $("#imgMenuOptions").width($("#imgMenuOptions").width() + 6);
        $("#imgMenuOptions").height($("#imgMenuOptions").height() + 6);
        ShowOptions();        
    }    

    menuOptionsDepressed = false;
}

function MenuHelpDown()
{
    $("#imgMenuHelp").css("border", "4px solid #AAAAAA");
    $("#imgMenuHelp").width($("#imgMenuHelp").width() - 6);
    $("#imgMenuHelp").height($("#imgMenuHelp").height() - 6);
    menuHelpDepressed = true;
}

function MenuHelpUp()
{
    if (menuHelpDepressed)
    {
        $("#imgMenuHelp").css("border", "1px solid white");
        $("#imgMenuHelp").width($("#imgMenuHelp").width() + 6);
        $("#imgMenuHelp").height($("#imgMenuHelp").height() + 6);
        ShowHelp();
    }

    menuHelpDepressed = false;
}

function TempDisableButton($button, time)
{
    if(!$button.data("disabled"))
    {
        $button.data("disabled", true);
        if ($button[0].src && $button[0].src.indexOf("_disabled") == -1)
        {
            $button[0].src = $button[0].src.replace(".png", "_disabled.png");
        }

        setTimeout(function ()
        {
            $button.data("disabled", false);
            if ($button[0].src && $button[0].src.indexOf("_disabled") != -1)
            {
                $button[0].src = $button[0].src.replace("_disabled.png", ".png");
            }
        }, time);
    }
}

function EnableNextLevel()
{
    document.getElementById("imgNextLevel").addEventListener("touchstart", nextLevelButtonDown, false);
    document.getElementById("imgNextLevel").addEventListener("touchend", nextLevelButtonUp, false);
    document.getElementById("imgNextLevel").addEventListener("mouseup", nextLevelButtonUp, false);
    document.getElementById("imgNextLevel").addEventListener("mousedown", nextLevelButtonDown, false);
    nextLevelButtonEnabled = true;
}

function DisableNextLevel()
{
    document.getElementById("imgNextLevel").removeEventListener("touchstart", nextLevelButtonDown, false);
    document.getElementById("imgNextLevel").removeEventListener("touchend", nextLevelButtonUp, false);
    document.getElementById("imgNextLevel").removeEventListener("mouseup", nextLevelButtonUp, false);
    document.getElementById("imgNextLevel").removeEventListener("mousedown", nextLevelButtonDown, false);
    nextLevelButtonEnabled = false;
}

function EnableMenuButton()
{
    document.getElementById("imgMenu").addEventListener("touchstart", MenuButtonDown, false);
    document.getElementById("imgMenu").addEventListener("touchend", MenuButtonUp, false);
    document.getElementById("imgMenu").addEventListener("mouseup", MenuButtonUp, false);
    document.getElementById("imgMenu").addEventListener("mousedown", MenuButtonDown, false);
    menuButtonEnabled = true;
    $("#imgMenu")[0].src = "img/menu.png";
}

function DisableMenuButton()
{
    document.getElementById("imgMenu").removeEventListener("touchstart", MenuButtonDown, false);
    document.getElementById("imgMenu").removeEventListener("touchend", MenuButtonUp, false);
    document.getElementById("imgMenu").removeEventListener("mouseup", MenuButtonUp, false);
    document.getElementById("imgMenu").removeEventListener("mousedown", MenuButtonDown, false);
    menuButtonEnabled = false;
    $("#imgMenu")[0].src = "img/menu_disabled.png";
}

function PauseButtonUp()
{
    if (!$("#imgPause").data("disabled"))
    {
        if (!$("#imgPause").data("paused"))
        {
            $("#imgPause")[0].src = "img/resume_disabled.png";
            clearTimeout(showWinScreenTimeout);
            clearTimeout(openWidthTimeout);
            clearTimeout(getNameTimeOut);
            clearTimeout(showHighScoresTimeout);
            clearTimeout(hideAllTimeout);
            clearTimeout(resumeGameTimeout);
            ShowMenuScreen();
            $("#imgPause").data("paused", true);
        }
        else
        {
            $("#imgPause")[0].src = "img/pause_disabled.png";
            clearTimeout(showWinScreenTimeout);
            clearTimeout(openWidthTimeout);
            clearTimeout(getNameTimeOut);
            clearTimeout(showHighScoresTimeout);
            clearTimeout(hideAllTimeout);
            clearTimeout(resumeGameTimeout);
            HideWinScreen(function () { });
            $("#imgPause").data("paused", false);
            resumeGameTimeout = setTimeout(function ()
            {
                Run = GameRun;
                PlayMusic();
                init = true;
                Run();
            }, 1000);
        }

        TempDisableButton($("#imgPause"), 1000);
    }
}

function PauseButtonDown()
{
    if (!$("#imgPause").data("disabled"))
    {
        if ($("#imgPause")[0].src.search("img/pause") > -1)
        {
            $("#imgPause")[0].src = "img/pause2.png";
        }
        else
        {
            $("#imgPause")[0].src = "img/resume2.png";
        }
    }
}

function PlayAgainButtonDown()
{
	$("#imgPlayAgain")[0].src = "img/play_again2.png";
}

function PlayAgainButtonUp()
{
    $("#imgPlayAgain")[0].src = "img/play_again.png";
    
    if (PreventDoubleClick(2000, $("#imgPlayAgain")))
    {
        init = false;
        ReplayLevel();        
        TempDisableButton($("#imgPause"), 1000);
        $("#imgPause").data("paused", false);
    }            
}

function PreventDoubleClick(delay, $element)
{
    if($element.data("preventDoubleClick"))    
    {
        return false;
    }

    $element.data("preventDoubleClick", true);
    if ($element[0].src && $element[0].src.indexOf("_disabled") == -1)
    {
        $element[0].src = $element[0].src.replace(".png", "_disabled.png");
    }

    setTimeout(function ()
    {
        $element.data("preventDoubleClick", false);
        if ($element[0].src && $element[0].src.indexOf("_disabled") != -1)
        {
            $element[0].src = $element[0].src.replace("_disabled.png", ".png");
        }
    }, delay);

    return true;
}

var nextLevelButtonDown = function NextLevelButtonDown()
{
    if (nextLevelButtonEnabled)
        $("#imgNextLevel")[0].src = "img/next_level2.png";
}

var nextLevelButtonUp = function NextLevelButtonUp()
{
    if (nextLevelButtonEnabled)
    {
        $("#imgNextLevel")[0].src = "img/next_level.png";
        if (PreventDoubleClick(5000, $("#imgNextLevel")))
            NextLevel();
    }    
}

function MenuButtonDown()
{
	$("#imgMenu")[0].src = "img/menu2.png";
}

function MenuButtonUp()
{
    clearTimeout(showWinScreenTimeout);
    clearTimeout(openWidthTimeout);
    clearTimeout(getNameTimeOut);
    clearTimeout(showHighScoresTimeout);
    clearTimeout(hideAllTimeout);

    DisableNextLevel();
    DisableMenuButton();
    $("#divMainMenu").show();
    $("#divMainMenuBackground").show();    
}

function TouchLeft(e)
{
    KeyDown({ keyCode: 37 });
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
}

function TouchUp(e)
{
    KeyDown({ keyCode: 38 });
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
}

function TouchDown(e)
{
    KeyDown({ keyCode: 40 });
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
}

function TouchRight(e)
{
    KeyDown({ keyCode: 39 });
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
}

function TouchLeftEnd(e)
{
    KeyUp({ keyCode: 37 });
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
}

function TouchUpEnd(e)
{
    KeyUp({ keyCode: 38 });
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
}

function TouchDownEnd(e)
{
    KeyUp({ keyCode: 40 });
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
}

function TouchRightEnd(e)
{
    KeyUp({ keyCode: 39 });
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
}

var rotateDelay = 100;
var rotateTime = 0;
var rotateCount = 0;
function KeyDown(e)
{
    slideLeft = false;
    slideRight = false;
	switch(e.keyCode)
	{
		case 37:
			//Left Key		
			horizontalSpeed = -horizontalStandardSpeed;
			break;
		case 38:
		    //Up Key
		    var d = new Date();
		    var _t = d.getTime();
		    if (rotateCount == 0 && _t - rotateTime > rotateDelay)
		    {
		        rotateTime = _t;
		        Rotate();
		        rotateCount = 1;
		    }
			
			break;
		case 39:
			//Right Key
			horizontalSpeed = horizontalStandardSpeed;
			break;
		case 40:
			//Down Key
		    verticalSpeed = horizontalStandardSpeed;		    
			break;
    }
}

function KeyUp(e)
{	
	switch(e.keyCode)
	{
		case 37:
			//Left Key		
			horizontalSpeed = 0;
			break;
		case 38:
			//Up Key
		    rotateCount = 0;
			break;
		case 39:
			//Right Key
			horizontalSpeed = 0;
			break;
		case 40:
			//Down Key
			verticalSpeed = verticalStandardSpeed;			
			break;
	    case 73:
            //i key
	       // $("#divImportCustomLevel").show();
	        break;
	}
}

function TouchPadMouseMove(e)
{
	lastMouseX = mouseX;
	lastMouseY = mouseY;
	 
	if(typeof e.targetTouches === "undefined")
	{
		mouseX = e.pageX;
		mouseY = e.pageY;
	}
	else
	{
		var touch = e.targetTouches[0];
		mouseX = touch.pageX;
		mouseY = touch.pageY;
	}
		
	/*if(e.offsetX) 
	{
		mouseX = e.offsetX;
		mouseY = e.offsetY;
	}
	else if(e.layerX) 
	{
		mouseX = e.layerX;
		mouseY = e.layerY;
	}*/

	if(mouseDown)
	{
		var pos = $("#divTouchPad").position();
		
		$("#divTouchPad").css({ 
			left: pos.left + (mouseX - lastMouseX),
			top: pos.top + (mouseY - lastMouseY)
		});
	}
}

function TouchPadMouseDown(e)
{
	mouseDown = true;
	
	if(typeof e.targetTouches === "undefined")
	{
		mouseX = e.pageX;
		mouseY = e.pageY;
	}
	else
	{
		var touch = e.targetTouches[0];
		mouseX = touch.pageX;
		mouseY = touch.pageY;
	}
	
	lastMouseX = mouseX;
	lastMouseY = mouseY;
}

function TouchPadMouseUp(e)
{
	mouseDown = false;
}

function RunCustomLevel()
{
    level = JSON.parse($("#txtCustomLevel").val());
    $("#divImportCustomLevel").hide();
    NextLevel(true);
}

function CancelCustomLevel()
{
    $("#divImportCustomLevel").hide();
}

function ClearCustomLevel()
{
    $("#txtCustomLevel").val("");
}

function ResetAllButtons()
{
    $("#imgPlayAgain")[0].src = "img/play_again.png"; 
    
    if ($("#imgPause")[0].src.search("img/pause") > -1)
    {
        $("#imgPause")[0].src = $("#imgPause")[0].src.replace("pause2", "pause");
    }
    else
    {
        //$("#imgPause")[0].src = "img/resume.png";
        $("#imgPause")[0].src = $("#imgPause")[0].src.replace("resume2", "resume");
    }

    if (menuButtonEnabled)
    {
        $("#imgMenu")[0].src = "img/menu.png";
    }
    else
    {
        $("#imgMenu")[0].src = "img/menu_disabled.png";
    }

    if (nextLevelButtonEnabled)
    {
        $("#imgNextLevel")[0].src = "img/next_level.png";
    }
    else
    {
        $("#imgNextLevel")[0].src = "img/next_level_disabled.png";
    }

    if (menuPlayDepressed)
    {
        $("#imgMenuPlay").css("border", "1px solid white");
        $("#imgMenuPlay").width($("#imgMenuPlay").width() + 6);
        $("#imgMenuPlay").height($("#imgMenuPlay").height() + 6);
    }

    if (menuOptionsDepressed)
    {
        $("#imgMenuOptions").css("border", "1px solid white");
        $("#imgMenuOptions").width($("#imgMenuOptions").width() + 6);
        $("#imgMenuOptions").height($("#imgMenuOptions").height() + 6);
    }

    if (menuHelpDepressed)
    {
        $("#imgMenuHelp").css("border", "1px solid white");
        $("#imgMenuHelp").width($("#imgMenuHelp").width() + 6);
        $("#imgMenuHelp").height($("#imgMenuHelp").height() + 6);
    }

    menuOptionsDepressed = false;
    menuPlayDepressed = false;
    menuHelpDepressed = false;

    $("#divCloseOptionsButton").css("border", "1px solid white");

    $(".classSpanButton").css("background-color", "");

}

function IncreaseChapter()
{
    SetChapter(1);
}

function DecreaseChapter()
{
    SetChapter(-1);
}

function SetChapter(count)
{
    chapter += count;
    InitLevelSelection();
}

function InitLevelSelection()
{
    $("#divLevelsMenu").show();

    if(chapter - 1 >= 0)
    {        
        $("#imgDecreaseChapter")
            .off("click", DecreaseChapter)
            .on("click", DecreaseChapter)
            [0].src = "img/left_arrow.png";
    }
    else
    {
        $("#imgDecreaseChapter")
            .off("click", DecreaseChapter)
            [0].src = "img/left_arrow_disabled.png";
    }

    if ((chapter + 1) * chapterSize < levels.length && levels[(chapter + 1) * chapterSize].unlocked)
    {
        $("#imgIncreaseChapter")
            .off("click", IncreaseChapter)
            .on("click", IncreaseChapter)
            [0].src = "img/right_arrow.png";
    }
    else
    {
        $("#imgIncreaseChapter")
            .off("click", IncreaseChapter)
            [0].src = "img/right_arrow_disabled.png";
    }
        
    $("#spanChapter").html(chapter + 1);
    var str = "<table><tr>";
    var cnt = 0;    
    for (var i = chapter * chapterSize; i < levels.length && cnt < chapterSize; i++)
    {
        cnt++;
        if (levels[i].unlocked)
        {
            str += "<td class='tdUnlocked' >" + (i + 1) + "</td>";
        }
        else
        {
            str += "<td class='divLockedLevel'>&nbsp;</td>";
        }

        if (i % 5 == 4)
            str += "</tr>";
    }
    str += "</tr></table>";

    var levelHeight = Math.floor($("#divLevelsContainer").height() / 5);
    var fontSize = Math.floor(levelHeight * .45);

    $("#divLevelsMenuHeader")
        .css("font-size", fontSize + "pt")
        .height(fontSize + 5);
    $("#divLevelsMenuHeader img")
        .width(fontSize + 5)
        .height(fontSize + 5);

    $("#divLevelsContainer").html(str);
    $(".divLockedLevel").css("background-image", "url(./img/locked.png)");
    var clicklevel = function ()
    {
        levelNumberClicked = +$(this).html() - 1;
        var offset = $(this).offset();
        $("#divClickedCell").width($(this).width());
        $("#divClickedCell").height($(this).height());
        $("#divClickedCell").css("left", offset.left);
        $("#divClickedCell").css("top", offset.top);
        $("#divClickedCell").show();
        $("#divClickedCell").on("mouseup", function myfunc()
        {
            $("#divClickedCell").hide();
            $("#divClickedCell").off("mouseup", myfunc);
            SelectLevel(levelNumberClicked);
        });
    };

    $("#divLevelsContainer .tdUnlocked")
        .css("font-size", fontSize + "pt")
        .on("touchstart", clicklevel)
        .on("mousedown", clicklevel);
    $("#divLevelsContainer .divLockedLevel")
        .css("font-size", fontSize + "pt");

    var $td = $("#divLevelsContainer td");
    var ratio = $td.width() / $td.height();
    if(ratio > 1)
    {
        $(".divLockedLevel").css("background-size", Math.floor((1 / ratio) * 100) + "% 100%");
        $(".tdUnlocked").css("background-size", Math.floor((1 / ratio) * 100) + "% 100%");
    }
    else
    {
        $(".divLockedLevel").css("background-size", "100% " + Math.floor((ratio) * 100) + "%");
        $(".tdUnlocked").css("background-size", "100% " + Math.floor((ratio) * 100) + "%");
    }

    if(localStorage)
    {
        localStorage.lastChapter = chapter;
    }
}

function SelectLevel(number)
{
    levelNumber = number + 1;
    level = levels[number];

    $("#divLevelsMenu").hide();
    $("#divMainMenu").hide();
    $("#divMainMenuBackground").hide();
    $("#divHighscores").hide();
    $("#divGetName").hide();
    $("#imgPause").show();
    $("#imgPause")[0].src = "img/pause.png";
    $("#divLevel").html("Level " + levelNumber);

    EnableMenuButton();
    ResetLevel();
    init = false;
    InitLevel(true);
    init = true;
    Run = GameRun;
    PlayMusic();
    $("#divGameWon").css("opacity", 0);
    $("#divGameWon").hide();
    $("#divMenuButtons").css("width", 0);
    $("#divMenuButtons").hide();
    $("#divHighscores").hide();
    $("#imgPause").data("paused", false);
    Run();
}

function ShowOptions()
{
    var fontSize = Math.floor($(window).height() * .1);
    var fontSize2 = Math.floor(fontSize * .5);
    var fontSize3 = Math.floor(fontSize * .35);

    var musicStr = "";
    for (var i = 0; i < volumeTicks; i++)
    {
        musicStr += "<div id='divVolumeTick" + i + "' class='classVolumeTick' ><div>&nbsp;</div></div>";
    }
    $("#divOptions").width($(window).width() - 10);

    if ($(window).height() < 300)
    {
        $("#divOptionsTitle").hide();
        $("#divOptionsTitleSpace").show();
    }

    $("#divOptionsVolume").html(musicStr);
    $(".classVolumeTick").on("click", SetVolume);

    $(".classOptionsTitle")
        .css("font-size", fontSize);

    $("#divOptions")
        .css("font-size", fontSize2)
        .show();

    $(".classOptionsText")
        .css("font-size", fontSize3);

    var smallSize = GetSize(buttonSizeSmall);
    var mediumSize = GetSize(buttonSizeMedium);
    var largeSize = GetSize(buttonSizeLarge);

    $("#tdKeypadSmall > img")
        .width(smallSize)
        .height(smallSize)
        .css("margin-top", ((largeSize - smallSize) / 3) + "px");
            
    $("#tdKeypadMedium > img")
        .width(mediumSize)
        .height(mediumSize)
         .css("margin-top", ((largeSize - mediumSize) / 3) + "px");
            
    $("#tdKeypadLarge > img")
        .width(largeSize)
        .height(largeSize);

    var containerSize = Math.floor(largeSize * 1.2);

    $(".classTableImageButton td, .classGraphicsLevel")
        .width(containerSize)
        .height(containerSize);   

    if($(".classImgGraphicsRes").offset().top + $(".classImgGraphicsRes").height() + 20 > $(window).height() )
    {
        $(".classImgGraphicsRes").hide();
        $("#tdKeypadSmall > img").hide();
        $("#tdKeypadMedium > img").hide();
        $("#tdKeypadLarge > img").hide();
        $("#tdKeyPadTwoHands > img").hide();
        $("#tdKeyPadLeftHand > img").hide();
        $("#tdKeyPadRightHand > img").hide();
        $(".classKeyPad").width(fontSize * 2);
        $(".classKeyPad").height(fontSize);

        $(".classGraphicsLevel").height(fontSize);
        $(".classGraphicsLevel").width(fontSize * 2);
    }

    UpdateMusicVolume();
    SetKeyPadSelect();
    SetKeyPadStyleSelect();
    SetGraphicsLevelSelect();

    if (musicOn)
        SetMusicOn();
    else
        SetMusicOff();
}

function SetVolume(e)
{
    var len = "divVolumeTick".length;
    var index = parseInt(e.currentTarget.id.substring(len, e.currentTarget.id.length));
    musicVolume = index / volumeTicks;
    UpdateMusicVolume();
}

function UpdateMusicVolume()
{
    var index = Math.floor(volumeTicks * musicVolume);
    for (var i = index; i >= 0; i--)
    {
        $("#divVolumeTick" + i + " > div")
            .css("background-color", "blue");
    }

    for(var i = volumeTicks; i > index; i--)
    {
        $("#divVolumeTick" + i + " > div")
            .css("background-color", "white");
    }
}

function SetMusicOff()
{
    musicOn = false;
    //$("#divOptionsVolume > div > div").css("background-color", "#343434");
    
    var $divCoverVolume = $("#divCoverOptionsVolume")
        .css("position", "absolute")
        .css("background-color", "rgba(0,0,0,0.5)")
        .css("left", $("#divOptionsVolume").offset().left)
        .css("top", $("#divOptionsVolume").offset().top)
        .width($("#divOptionsVolume").width())
        .height($("#divOptionsVolume").height() + 10)
        .show();
   
    $("#spanMusicOnButton").css("border", "1px solid white");
    $("#spanMusicOffButton").css("border", "3px solid blue");
}

function SetMusicOn()
{
    musicOn = true;
    $("#divCoverOptionsVolume").hide();
    $("#spanMusicOnButton").css("border", "3px solid blue");
    $("#spanMusicOffButton").css("border", "1px solid white");
}

function SetKeyPad(size)
{
    if (size == 'small')
        buttonSize = buttonSizeSmall;
    else if (size == 'medium')
        buttonSize = buttonSizeMedium;
    else
        buttonSize = buttonSizeLarge;

    ConfigureSizes();
    SetKeyPadSelect();
}

function SetKeyPadSelect()
{
    var $element;

    switch (buttonSize)
    {
        case buttonSizeSmall:
            $element = $("#tdKeypadSmall");
            break;
        case buttonSizeMedium:
            $element = $("#tdKeypadMedium");
            break;
        case buttonSizeLarge:
            $element = $("#tdKeypadLarge");
            break;
    }

    var offset = $element.offset();
    $("#divClickedCell").width($element.width() - 5);
    $("#divClickedCell").height($element.height() - 5);
    $("#divClickedCell").css("left", offset.left);
    $("#divClickedCell").css("top", offset.top);
    $("#divClickedCell").show();
}

function SetKeyPadStyle(style)
{
    keypadStyle = style;
    ConfigureSizes();
    SetKeyPadStyleSelect();
}

function SetKeyPadStyleSelect()
{
    var $element;
    switch(keypadStyle)
    {
        case "two hands":
            $element = $("#tdKeyPadTwoHands");
            break;
        case "left hand":
            $element = $("#tdKeyPadLeftHand");
            break;
        case "right hand":
            $element = $("#tdKeyPadRightHand");
            break;
    }
    var offset = $element.offset();
    $("#divClickedCell2").width($element.width() - 5);
    $("#divClickedCell2").height($element.height() - 5);
    $("#divClickedCell2").css("left", offset.left);
    $("#divClickedCell2").css("top", offset.top);
    $("#divClickedCell2").show();
}

function SetGraphicsLevel(graphicsLevel)
{
    GraphicsRes = graphicsLevel;
    SetGraphicsLevelSelect();
    //InitLevel(false);
}

function SetGraphicsLevelSelect()
{
    var $element;

    if(GraphicsRes == "low")
    {
        $element = $("#tdGraphicsLevelLow");
    }
    else
    {
        $element = $("#tdGraphicsLevelHigh");
    }
    var offset = $element.offset();
    $("#divClickedCell3").width($element.width() - 5);
    $("#divClickedCell3").height($element.height() - 5);
    $("#divClickedCell3").css("left", offset.left);
    $("#divClickedCell3").css("top", offset.top);
    $("#divClickedCell3").show();
}

function CloseOptionsMouseDown()
{
    $("#divCloseOptionsButton").css("border", "3px solid white");
}

function CloseOptions()
{
    $("#divCloseOptionsButton").css("border", "1px solid white");
    $("#divOptions").hide();
    $("#divClickedCell").hide();
    $("#divClickedCell2").hide();
    $("#divClickedCell3").hide();

    if(localStorage)
    {
        localStorage.GraphicsRes = GraphicsRes;
        localStorage.buttonSize = buttonSize;
        localStorage.musicVolume = musicVolume;
        localStorage.keypadStyle = keypadStyle;
        localStorage.musicOn = musicOn;
    }
}


var helpPageNumber = 1;
function ShowHelp()
{
    var font_size = Math.floor($(window).height() / 20);
    var max_font_size = 39;
    var min_font_size = 12;

    if (font_size > max_font_size)
        font_size = max_font_size;

    if (font_size < min_font_size)
        font_size = min_font_size;

    $("#divHelp")
        .css("font-size", font_size + "px")
        .show();

    $("#divHelp img").height(font_size * 8 + "px");
    $("#imgHelpPieces").height(font_size * 4 + "px");
    $("#imgHelpSliding").height(font_size * 12 + "px");
    
    var $helpPages = $(".classHelpPage");

    $helpPages
        .width($(window).width() - 5)
        .height($(window).height() - font_size * 2 - 10)       
        .hide();
    
    $("#spanHelpPageNumber").html(helpPageNumber + " of " + $helpPages.length);

    $("#divHelpPage" + helpPageNumber).show();

    var disabledColor = "#333333";

    if (helpPageNumber > 1)
    {
        $("#spanHelpButtonLeft")
            .off("click", PreviousHelpPage)
            .on("click", PreviousHelpPage)
            .css("color", "white")
            .css("border-color", "white");
    }
    else
    {
        $("#spanHelpButtonLeft")
            .off("click", PreviousHelpPage)
            .css("color", "#555555")
            .css("border-color", disabledColor);
    }
      
    if (helpPageNumber < $helpPages.length)
    {
        $("#spanHelpButtonRight")           
            .off("click", NextHelpPage)
            .on("click", NextHelpPage)
            .css("color", "white")
            .css("border-color", "white");
    }
    else
    {
        $("#spanHelpButtonRight")
            .off("click", NextHelpPage)
            .css("color", disabledColor)
            .css("border-color", disabledColor);
    }

    $(".classSpanButton")
        .off("mousedown", SpanButtonDown)
        .off("mouseup", SpanButtonUp)
        .on("mousedown", SpanButtonDown)
        .on("mouseup", SpanButtonUp);
}

function CloseHelp()
{
    $("#divHelp").hide();
}

function PreviousHelpPage(e)
{
    helpPageNumber--;
    ShowHelp();
}

function NextHelpPage(e)
{
    helpPageNumber++;
    ShowHelp()
}

function SpanButtonDown(e)
{
    $(e.target).css("background-color", "#888888");
}

function SpanButtonUp(e)
{
    $(e.target).css("background-color", "");    
}

function ShowCredits()
{
    $("#divCredits").show();
}

function CloseCredits()
{
    $("#divCredits").hide();
}

function InitFireworks()
{
    
}

function ShowFireworks(message, bGameWon)
{
    var freq = 0.05;
    if (bGameWon)
        freq = 0.15;

    $("#divFireworks").show();
    var fontSize = Math.floor($(window).width() * .05);

    $("#divFireworksMessage")
        .css("font-size", fontSize)
        .css("left", 0)
        .css("top", 0)
        .html(message);

    fireWorksAnimation.Init($("#divFireworksContainer")[0], { zoom: 0.5, frequency: freq, sound: musicOn, volume: musicVolume });

    $("#divFireworksContainer").width("100%").height($(window).height() - $("#spanCloseFireworks").height() - 25);
	fireWorksAnimation.Start();
	$( document.body ).on( "click", CloseFireworks );
	$( document.body ).on( "keydown", CloseFireworks );
}

function CloseFireworks()
{
	$( document.body ).off( "click", CloseFireworks );
	$( document.body ).off( "keydown", CloseFireworks );
    fireWorksAnimation.Stop(function ()
    {
        $("#divFireworks").hide();
    });
}
