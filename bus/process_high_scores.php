<?php
	header("Access-Control-Allow-Origin: *");
	
	require("high_scores.php");
	require("connection.php");
	
	$connection = Connect();	
	
	switch($_GET["Action"])
	{
		case "SubmitScore":
			$Data = null;			
			$Game = "Maze";
			$IP = $_SERVER['REMOTE_ADDR'];		
			$Level = $_POST["Level"];
			$Score = $_POST["Score"];
			$Stuff = $_POST["Stuff"];
			$Name = $connection->real_escape_string($_POST["Name"]);
			$Name = StripNonAlphaNumeric($Name);
			
			if(ValidateName($Name) && ValidateStuff($Name, $Level, $Score, $Stuff))
			{
				SaveScores($Data, $Game, $IP, $Level, $Name, $Score, $connection);	
				echo $Name;
			}
			break;
		case "GetRanking":
			$Score = $_POST["Score"];
			$Level = $_POST["Level"];
			GetRanking($Score, $Level, $connection);
			break;
		case "GetTop10":
			$Ranking = $_POST["Ranking"];
			$Score = $_POST["Score"];
			$Level = $_POST["Level"];
			GetTop10($Ranking, $Score, $Level, $connection);
			break;
		case "TestTop10":
			$Ranking = 100;
			$Score = -55;
			$Level = 1;
			echo "Testing";
			GetTop10($Ranking, $Score, $Level, $connection);			
			break;
		case "TestValidateName":
			$Name = $connection->real_escape_string($_GET["Name"]);			
			$Name = StripNonAlphaNumeric($Name);
			if(!ValidateName($Name))
				echo "<br /> Curse found: $Name";
			else
				echo "<br /> Clean: $Name";
			break;
		case "TestValidateNames":
			TestValidateNames();
			break;
	}
	
	$connection->close();
?>